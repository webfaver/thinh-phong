<div class="container">
    <div class="col-md-4 text-left">
        Hiển thị {{$data->count()."/".$data->total()}} kết quả, từ {{ NULL !== $data->firstItem() | $data->firstItem() }} đến {{NULL !== $data->lastItem() | $data->lastItem()}}. 
    </div>
    <div class="col-md-8 text-right">
        {!! $data->links() !!}
    </div>
</div>

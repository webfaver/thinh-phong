@extends('layouts.auth')

@section('content')
<div id="login" class="animate form">
    <section class="login_content">
        <form role="form" method="POST" action="{{ url('/login') }}">
            <h1>Đăng nhập</h1>
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div>
                <input type="email" name="email" class="form-control" placeholder="Địa chỉ email" required="" value="{{ old('email') }}" />
            </div>
            <div>
                <input type="password" name="password" class="form-control" placeholder="Mật khẩu" required="" />
            </div>

            <div class="pull-left">
                <input type="checkbox" name="remember" id="rememberMe">
                <label for="rememberMe">Ghi nhớ tôi</label>
            </div>
            <div class="clearfix"></div>
            <div>
                <button type="submit" class="btn btn-default submit"><i class="fa fa-unlock"></i> Đăng nhập</button>
                <a class="reset_pass" href="{{ url('/password/email') }}">Quên mật khẩu?</a>
            </div>            
            <div class="clearfix"></div>
            <div class="separator">                
                <div class="clearfix"></div>
                <br />
                <div>
                    <h1><i class="fa fa-glass"></i> {{env('APP_NAME','coca.vn')}}</h1>

                    <p>©{{date('Y')}} All Rights Reserved</p>
                </div>
            </div>
        </form>
        <!-- form -->
    </section>
    <!-- content -->
</div>
@endsection

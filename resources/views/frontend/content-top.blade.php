<?php 
  $moduleId = CConfig::get('MODULE_HOME_SLIDER_1','1');
  $sliderItems = App\Models\Module::loadModule($moduleId);
?>
@if (count($sliderItems) > 0)
<div id="homeSlider">
  @foreach($sliderItems as $item)
  <div class="content item">
    <div class="col-lg-5 item-col-left">
      <div class="product-img">
        <img src="{{$item->getImgBySize(256)}}">
      </div>
    </div>
    <div class="col-lg-7 item-col-right">
      <h1><a href="{{route('productDetail',[$item->slug])}}">{{$item->name}}</a></h1>
      <p>
        <?php 
          $desc = strip_tags($item->desc);
          $ellipsis = strlen($desc) > 320 ? '...' : '';
        ?>
        {{mb_substr($desc,0,320,'UTF8').$ellipsis }}
      </p>
      <div class="tx-price">{{$item->fPrice()}}</div>
      <div class="btn-addCart openSize">
        <h4>Bạn Cần Mua?</h4>
        <p>yêu cầu thông tin</p>
      </div>
      <input type="hidden" value="{{json_encode(['id'=>$item->id, 'name'=>$item->name,'size'=>$item->sizes->toArray(),'url'=>$item->getImgBySize(128),'price'=>$item->price])}}">
    </div>
  </div>
  @endforeach
</div>
@endif
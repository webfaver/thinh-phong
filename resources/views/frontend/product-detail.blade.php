@extends('layouts.frontend')
@push('custom-style')
	<link rel="stylesheet" href="{{asset('packages/fancybox/source/jquery.fancybox.css')}}"> 
	<link rel="stylesheet" type="text/css" href="{{asset('packages/bxslider-4/dist/jquery.bxslider.min.css')}}">
	<style type="text/css">
		.showcase img{
			width: 350px;
		}
	</style>
@endpush
@section('title',$product->name)
@section('content')
	<div class="page page-product-detail">
		<!--end header-->
		<div class="padding-xs">	
			<div class="container">
				<div class="row breadcrumb">
					<ul class="breadcrumb-list">
						<li><i class="fa fa-home"></i> <a href="/">Home</a></li>
						<li><a href="{{route('collection',[$product->category->slug])}}">{{$product->category->name}}</a></li>
						<li>{{$product->name}}</li>
					</ul>
				</div>
			</div>		
		</div>
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-5 col-md-5 col-lg-7 product_page-left">
					<div id="default_gallery" class="product-gallery">
						<div class="image-thumb">
							<ul id="image-additional" class="image-additional">
								@foreach($product->images() as $img)
								<li>
									<a href="#" data-image="{{asset($img->getImgBySize(1024, true))}}">
										<img src="{{asset($img->getThumbBySize(88))}}" alt="" />
									</a>
								</li>
								@endforeach
							</ul>
						</div>
						<div id="product-image" class="product-image">
							  <img src="{{asset($product->getImgBySize(1024))}}" />
						</div>
					</div>                       
				</div>
				<div id="full-gallery" class="col-sm-8 col-sm-offset-2">
					<div class="full-slider">
						<div class="content item">
							<div class="col-xs-12 col-lg-5 mobile-gallery">
								<div class="product-img">
									<img src="{{asset($product->getImgBySize(360))}}">
								</div>
							</div>
						</div>
						<div class="content item">
							<div class="col-xs-12 col-lg-5 mobile-gallery">
								<div class="product-img">
									<img src="{{asset($product->getImgBySize(360))}}">
								</div>
							</div>
						</div>
						<div class="content item">
							<div class="col-xs-12 col-lg-5 mobile-gallery">
								<div class="product-img">
									<img src="{{asset($product->getImgBySize(360))}}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-7 col-md-offset-0 col-lg-5 col-lg-offset-0 product_page-right">
					<div class="general_info product-info">
						<h4 class="product-title">{{$product->name}}</h4>                
						<div class="price-section">
							<span class="price">{{$product->fPrice()}}</span>
							<div class="reward-block">
							</div>
						</div>
						<ul class="list-unstyled product-section">
							<li><strong>Thương hiệu:</strong>
								<a href="#">{{$product->brand}}</a>
							</li>
							<li><strong>Mã hàng:</strong> <span>{{$product->plu}}</span></li>
							<li><strong>Tình trạng: </strong>{!!$product->fStock()!!}
							</li>
						</ul>
						<div class="btn-addCart">
							<h4>Bạn Cần mua?</h4>
							<p>Yêu cầu thông tin</p>
						</div>
					</div>            
				</div>
			</div>
		</div>
		<div class="container padding-xs">
			<div class="description padding-xs">
				<div class="box-header">
					<h1 class="title">Mô tả chi tiết</h1>
				</div>
				{!!$product->strip_tags('desc')!!}
			</div>
				
			<div class="related-product padding-xs">
				<div class="feature">
					<div class="box-header">
						<h1>Sản phẩm liên quan</h1>
					</div>
					<div class="box-feature">
						@foreach($relatedProducts as $product)
							@include('frontend.item',['product'=>$product,'width'=>'col-xs-12 col-sm-6 col-md-3 col-lg-3'])
						@endforeach
					</div>
				</div>
			</div>
		</div>
		<!--end content-->
		@include ('frontend.footer')
	</div>
@endsection
@push('custom-script')
	<script type="text/javascript" src="{{asset('packages/bxslider-4/dist/jquery.bxslider.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('packages/fancybox/source/jquery.fancybox.js')}}"></script>
	<script type="text/javascript" src="{{asset('packages/elevatezoom/jquery.elevateZoom-2.2.3.min.js')}}"></script>
	<script type="text/javascript">
		$(document).ready(function () {
		
		 $('.full-slider').bxSlider({
			pager:false,
			controls: true,
			nextText: '<i class="fa fa-chevron-left"></i>',
            prevText: '<i class="fa fa-chevron-right"></i>',
			moveSlides: 1
		 })
        $('#image-additional').bxSlider({
            mode: 'vertical',
            pager: false,
            controls: true,
            slideMargin: 13,
            minSlides: 4,
            maxSlides: 4,
            slideWidth: 88,
            nextText: '<i class="fa fa-chevron-down"></i>',
            prevText: '<i class="fa fa-chevron-up"></i>',
            infiniteLoop: false,
            adaptiveHeight: true,
            moveSlides: 1
        });
        
        $("#product-image img").elevateZoom({
					gallery:'image-additional',
					zoomType: 'inner',
					scrollZoom: true,
					cursor: 'crosshair',
					easing:true,
					imageCrossfade: true,
					loadingIcon: "/frontend/images/ajax-loader.gif"
				});
				//pass the images to Fancybox
				$("#product-image img").bind("click", function(e) {
					var ez = $('#product-image img').data('elevateZoom');
					$.fancybox(ez.getGalleryList());
					return false;
				});
    });	
	</script>
@endpush
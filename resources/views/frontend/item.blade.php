<div class="{{$width or 'col-lg-3'}}">
  <div class="item-product grid" items-id="{{$product->id}}">
    <div class="product-img">
      <img src="{{asset($product->getImgBySize(256))}}">
    </div>
    <div class="product-detail">
      <a href="{{route('productDetail',[$product->slug])}}"><p class="product-name">{{$product->name}}</p></a>
      <p class="product-price">{{$product->fPrice()}}</p>
    </div>
    <div class="btn-addCart openSize">
      <h4>Bạn Cần Mua?</h4>
      <p>Yêu cầu thông tin</p>
    </div>
    <input type="hidden" value="{{json_encode(['id'=>$product->id, 'name'=>$product->name,'size'=>$product->sizes->toArray(),'url'=>$product->getImgBySize(128),'price'=>$product->price])}}">
  </div>
</div>
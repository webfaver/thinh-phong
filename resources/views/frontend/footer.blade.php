<footer>
  <div class="container padding-sm support">
    <div class="col-md-7 col-lg-8 left-panel">
        <div class="col-md-6 infomation">
            <h5>Infomation</h5>
            <a><p>About Us</p></a>
            <a><p>Delivery infomation</p></a>
            <a><p>Privacy Policy</p></a>
        </div>
        <div class="col-md-6 service">
            <h5>Customer Service</h5>
            <a><p>Contact Us</p></a>
            <a><p>Site Map</p></a>
            <a><p>Returns</p></a>
        </div>
        <div class="col-md-6 extras">
            <h5>Extras</h5>
            <a><p>About Us</p></a>
            <a><p>Delivery infomation</p></a>
            <a><p>Privacy Policy</p></a>
        </div>
        <div class="col-md-6 account">
            <h5>My Account</h5>
            <a><p>My Account</p></a>
            <a><p>Order history</p></a>
            <a><p>Newsletter</p></a>
        </div>
    </div>
    <div class="col-md-5 col-lg-4 fbBox">
        <div class="fb-page" data-href="https://www.facebook.com/ruoucaocap.vn/?fref=ts" data-tabs="timeline" data-width="400" data-height="400" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"></div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row">
      <!--<iframe style="pointer-events:none;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.7188387032193!2d106.66744111433663!3d10.756138762510151!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752efb706876dd%3A0x167f89ade9a3797c!2sC%C3%B4ng+Ty+Tnhh+Tm+Th%E1%BB%8Bnh+Phong+-+Cn!5e0!3m2!1svi!2s!4v1458646706826" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>-->
      <iframe style="pointer-events:none;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.3008754268944!2d106.65981931535623!3d10.788252261923944!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMTDCsDQ3JzE3LjciTiAxMDbCsDM5JzQzLjIiRQ!5e0!3m2!1svi!2s!4v1458877043935" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
  </div>
  <p style="font-size:10px;text-align: right;padding: 10px;margin:0;font-weight: 400;">Powered by <a href="http://webfaver.com/work">WebFaver.Com</a> Store service &#169 {{date('Y')}}</p>
</footer>
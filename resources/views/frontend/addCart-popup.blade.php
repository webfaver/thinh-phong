<div class="container-fluid addCart-popup">
  <div class="overlay"></div>
  <div class="option-box">
    <h1>Thông tin thêm</h1>
    <h4>Kích cỡ <span>*</span></h4>
    <div class="size-box">
      
    </div>
    <div class="quantity-box">
      <h4>Số lượng</h4>
      <input id="quantity" value="1" placeholder="Nhập số lượng" >
    </div>
    <div class="btn-addCart add">
      <i class="fa fa-floppy-o" aria-hidden='true'></i>
    </div>
    <input type="hidden" class="popup-data" value="">
  </div>
</div>
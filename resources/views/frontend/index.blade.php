@extends('layouts.frontend')
@section('title','Trang chủ')
@push('custom-style')
	<link rel="stylesheet" type="text/css" href="/packages/fancybox/source/jquery.fancybox.css">
	<link rel="stylesheet" type="text/css" href="/packages/bxslider-4/dist/jquery.bxslider.min.css">
@endpush
@section('content')	
	<div class="page page-main">
		<!--banner-->
		<div class="container-fluid banner">
			<div class="row">
				<div class="container">
					@include('frontend.content-top')
				</div>
			</div>
		</div>
		<!--promotion-->
		<div class="container-fluid">
			<div class="container">
				<div class="row collection">
					<div class="col-lg-4 banner1">
						<div class="item-box">
							<div class="item-img"></div>
							<div class="item-caption"></div>
						</div>
					</div>
					<div class="col-lg-4 banner2">
						<div class="item-box">
							<div class="item-img"></div>
							<div class="item-caption"></div>
						</div>
					</div>
					<div class="col-lg-4 banner3">
						<div class="item-box">
							<div class="item-img"></div>
							<div class="item-caption"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--product-->
		<div class="container-fluid padding-xs">
			<div class="container">
				<div class="col-lg-12" id="content">
					<div class="feature">
						<div class="box-header text-center">
							<h1>Đặc sắc</h1>
						</div>
						<div class="box-feature clearfix padding-xs">
							@foreach($featured as $product)
								@include('frontend.item',['product'=>$product,'width'=>'col-xs-12 col-sm-6 col-md-3 col-lg-3'])
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container-fluid padding-sm company">
			<div class="container">
				<div class="col-lg-12 parallax">
					<h3 class="text-center">Lorem ipsum dolor</h3>
					<div class="col-lg-4 text-center">
						<span>500</span>
						<h5>Lorem ipsum</h5>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
					</div>
					<div class="col-lg-4 text-center">
						<span>60</span>
						<h5>Lorem ipsum</h5>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
					</div>
					<div class="col-lg-4 text-center">
						<span>20</span>
						<h5>Lorem ipsum</h5>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
					</div>
				</div>
			</div>
		</div>
		@include ('frontend.footer')
	</div>
@endsection
@push('custom-script')
	<script type="text/javascript" src="/packages/fancybox/source/jquery.fancybox.js"></script>
	<script type="text/javascript" src="/packages/bxslider-4/dist/jquery.bxslider.min.js"></script>
	<script type="text/javascript">
		$('#homeSlider').bxSlider({pager: false});
	</script>
@endpush
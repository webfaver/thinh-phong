<div class="container-fluid header">
  <div class="row menu">
      <div class="container">	
          <ul class="col-md-8 menu-list">
              <a href="{{route('index')}}"><li>Home</li></a>
              <a href="#"><li>Wine</li></a>
              <a href="#"><li>About</li></a>
              <a href="#"><li>Contact</li></a>
          </ul>
          
          <div class="col-md-1 col-lg-1 cart">
            <div class="cart-box" id="cart-box">
              <!--<a href="{{route('cart')}}">-->
                <i class="fa fa-shopping-cart"></i>
                <span>0</span>
              <!--</a>-->
            </div>
            <div class="cartContent">
            <div class="cartTable">
              <div class='cart_box_content_item'>
                <p>Mời bạn chọn sản phẩm yêu thích</p>
              </div>
            </div>
              
              <div class="btn-action">
                <a class="viewCart" href="{{route('cart')}}">VIEW CART</a>
              </div>
            </div>
          </div>
      </div>
  </div>
  <!--mobile menu-->
  <div class="row mobile-header-nav">
    <button class="mobile-btn-menu">
      <span></span>
    </button>
    <div class="cart-box" id="mb-cartBox">
      <a href="{{route('cart')}}">
        <i class="fa fa-shopping-cart"></i>
        <span>0</span>
      </a>
    </div>
    <div class="mobile-menu-list">
      <a href="#"><i class="fa fa-cog"></i></a>
      <ul>
        <li><a href="/"><span>Home</span></a></li>
        <li><a href="#"><span>Wine</span></a></li>
        <li><a href="#"><span>About</span></a></li>
        <li><a href="#"><span>Contact</span></a></li>
      </ul>
    </div>
  </div>
  <div class="menu-nav">
    <ul class="mobile-menu">
      @foreach($menu as $item)
        <li>
          <a class="mainCategoryText" href="{{route('collection', [$item['slug']])}}">{{$item['name']}}
            @if (count($item['child']) > 0)
            <span class="submenu-toggle"></span>
            @endif
          </a>
          @if (count($item['child']) > 0)
          <ul class="mobile-submenu">
            @foreach($item['child'] as $child)
            <li><a href="{{route('collection',[$child['slug']])}}">{{$child['name']}}</a></li>
            @endforeach
          </ul>
          @endif
        </li>
      @endforeach
    </ul>
  </div>
  <!--end mobile menu-->
  <div class="row  store-contact">
      <div class="container">
          <div class="col-lg-3 contact-info">
              <p><i class="fa fa-phone"></i> {{CConfig::get('TU_VAN_1_TEN','Ms. Nhung')}}</p>
              <p>{{CConfig::get('TU_VAN_1_SDT','09 89 1111 82')}}</p>
              <p><i class="fa fa-phone"></i> {{CConfig::get('TU_VAN_2_TEN','Ms. Huệ')}}</p>
              <p>{{CConfig::get('TU_VAN_2_SDT','090 960 8888')}}</p>
          </div>
          <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-0 col-md-4 col-lg-offset-0 logo">
              <a href="/"><img src="/frontend/images/logo.png" /></a>
          </div>
          <div class="col-xs-12 col-md-4 col-lg-4 search-place">
            <form class="clearfix" method="GET" action="../search">
              <input class="row" placeholder="Tìm kiếm" id="search-bar" name="name">
              <button id="btn-search"><i class="fa fa-search"></i></button>
            </form>
          </div>
      </div>
  </div>
  <div class="row store-category">
      <div class="container">
          <ul id="category" class="clearfix">
            @foreach($menu as $item)
              <li>
                <a class="mainCategoryText" href="{{route('collection', [$item['slug']])}}">{{$item['name']}}</a>
                @if (count($item['child']) > 0)
                <ul class="subCategory">
                  @foreach($item['child'] as $child)
                  <li><a href="{{route('collection',[$child['slug']])}}">{{$child['name']}}</a></li>
                  @endforeach
                </ul>
                @endif
              </li>
            @endforeach
          </ul>
      </div>
  </div>
</div>
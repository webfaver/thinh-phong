<div class="{{$width or 'col-lg-12'}}">
  <div class="item-product list" items-id="{{$product->id}}">
    <div class="product-img col-lg-4">
      <img src="{{asset($product->getImgBySize(256))}}">
    </div>
    <div class="product-detail col-lg-4">
      <a href="{{route('productDetail',[$product->slug])}}"><p class="product-name">{{$product->name}}</p></a>
      <p class="product-desc">
        <?php 
          $desc = strip_tags($product->desc);
          $ellipsis = strlen($desc) > 320 ? '...' : '';
        ?>
        {{mb_substr($desc,0,320,'UTF8').$ellipsis }}
      </p>
      <p class="product-price">{{$product->fPrice()}}</p>
    </div>
    <div class="col-lg-4 btn-addCart openSize">
      <h4>Bạn Cần Mua?</h4>
      <p>Yêu cầu thông tin</p>
    </div>
    <input type="hidden" value="{{json_encode(['id'=>$product->id, 'name'=>$product->name,'size'=>$product->sizes->toArray(),'url'=>$product->getImgBySize(128),'price'=>$product->price])}}">
  </div>
</div>
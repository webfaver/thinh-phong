@extends('layouts.frontend')
@section('title','Bo suu tap')
@section('content')
  <div class="page page-cart">
    <!-- breadcrumb -->
    <div class="padding-xs">  
      <div class="container">
        <div class="col-lg-12 breadcrumb">
          <ul class="breadcrumb-list">
            <li><i class="fa fa-home"></i> <a href="/">Home</a></li>
            <li>Phiếu yêu cầu thông tin</li>
          </ul>
        </div>
      </div>    
    </div>
    <!-- end breadcrumb -->
    <!-- content -->
    <div class="container">
      <div class="cart-wrapper">
        @if (session('status'))
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <i class="fa fa-check"></i>{{ session('status') }}
            </div>  
            @push('custom-script')
              <script type="text/javascript">
                $(function(){
                  localStorage.setItem('items','[]');
                });
              </script>
            @endpush
        @else
          <h1>
            <i class="fa fa-shopping-cart"></i> Phiếu yêu cầu thông tin
          </h1>
          <form class="shoping_cart" action="{{route('order')}}" method="post">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <div class="table-responsive">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th>Hình ảnh</th>
                    <th class="text-left">Sản phẩm</th>
                    <th class="text-left">Số lượng</th>
                    <th class="text-right">Đơn giá</th>
                    <th style="width: 0"></th>
                  </tr>
                </thead>
                <tbody>
                  <tr id="cart-template">
                    <td class="text-center vcenter">
                      <div class="image">
                        <a href="">
                          <img class="img-thumbnail" src="/frontend/images/bottle.png">
                        </a>
                      </div>
                    </td>
                    <td class="text-left">
                      <a class="link" href="">
                        <span class="name" name="productName">Armand de Brignac Blanc de Blancs</span>
                      </a>
                      <br>
                      <small>
                        <strong>Model</strong>:
                        <span class="model" name="productModel">Product 1</span>
                      </small>
                      <br>
                      <small>
                        <strong>Size</strong>:
                        <span class="size" name="productSize">Small</span>
                      </small>
                      <input type="hidden" class="productId" name="product_id[]" value="">
                      <input type="hidden" class="sizeId" name="size_id[]" value="">
                    </td>
                    <td class="text-left">
                      <div class="btn-block clearfix" style="min-width: 146px; max-width: 200px;">
                        <a class="quantity_decrease"><i class="fa fa-minus"></i></a>
                        <input class="quantity" type="text" name="quantity[]" value="1" size="1" min="1">
                        <a class="quantity_increase"><i class="fa fa-plus"></i></a>
                        <p>
                        </p>
                      </div>
                    </td>
                    <td class="text-right">
                      <div class="price price-product" name="productPrice">$118</div>
                    </td>
                    <td class="text-center cart-delete">
                      <a class="btn-cart-delete"><i class="fa fa-trash-o"></i></a>
                    </td>
                  </tr>
                </tbody>               
              </table>

            <!-- CUSTOMER INFOMATION -->
            @if ($errors->count() > 0)
              <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <ul>
                <?php $labels = App\Models\Order::getLabels()?>
                @foreach ($errors->getBag('default')->getMessages() as $key => $error)
                  <li><strong>{{$labels[$key]}}:</strong> {{ $error[0] }}</li>
                @endforeach
                </ul>
              </div>
            @endif
            <div class="form-group padding-xs">
              <label for="name">Họ tên</label>
              <input type="text" class="form-control" id="name" placeholder="Nguyễn Văn A" name="name" value="{{old('name')}}">
            </div>
            <div class="form-group">
              <label for="phone_number">Số điện thoại</label>
              <input type="text" class="form-control" id="phone_number" placeholder="093.XXXX.xxx" name="phone" value="{{old('phone')}}">
            </div>
            <div class="form-group">
              <label for="email">Email</label>
              <input type="email" class="form-control" id="email" placeholder="email" value="{{old('email')}}">
            </div>
            <div class="form-group">
              <label for="address">Địa chỉ</label>
              <input type="text" class="form-control" id="address" placeholder="địa chỉ" name="address" value="{{old('address')}}">
            </div>  
            <button type="submit" class="btn-addCart"><i class="fa fa-shopping-cart"></i> Gửi thông tin</button>
          </form>
        @endif
      </div>
    </div>
    <!--end content-->
    @include ('frontend.footer')
  </div>
@endsection
@push('custom-script')
  <script type="text/javascript" src="/packages/ddslick/jquery.ddslick.min.js"></script>
  <script type="text/javascript">
    $(function(){
      $('#sort-type').ddslick({
        width: 160
      });
      $('#range').ddslick({
        width: 50
      });
    });
  </script>
  <script>
    $('.cart').hide();

    var tmpHTML1 = $('#cart-template').clone();
    addCartItems();
    
    function addCartItems() {
      $('.table tbody').html("");
      items = JSON.parse(localStorage.getItem("items"));
      if (items != '' || items !== []) {
        $('.table tbody').append(tmpHTML1);
        for (var i=0;i<items.length;i++) {
          var tmpHTML = $('#cart-template').clone();

          var product = items[i].item;
          
          $(tmpHTML).addClass("cart-product").attr('item-id',product.id).attr('item-size',items[i].size).removeAttr('id');
          $(tmpHTML).find('.btn-cart-delete').attr('item-id',product.id);
          $(tmpHTML).find('.btn-cart-delete').attr('item-size', items[i].size);
          $(tmpHTML).find('.name').text(product.name);
          $(tmpHTML).find('.model').text(product.id);
          $(tmpHTML).find('.productId').val(product.id);
          $(tmpHTML).find('.size').text(items[i].sizeName);
          $(tmpHTML).find('.sizeId').val(items[i].size);          
          $(tmpHTML).find('.price-product').text(product.price == 0 ? "Liên hệ" : product.price);
          $(tmpHTML).find('.quantity').val(items[i].quantity);
          $(tmpHTML).find('.img-thumbnail').attr("src", product.url);
          
          $('.table tbody').append(tmpHTML);
  
        }
        
      }
    }
    $('.btn-cart-delete').click(function(){
      var id = $(this).attr('item-id');
      var sizeId = $(this).attr('item-size');
      deleteCartItem(id,sizeId);
    })
    
    $('.btn-block').on('click','.quantity_decrease',function(){
      var num = 0;
      var price = $(this).parents('.cart-product').find('.price-product').text();
      
      num -=1;
      var quant = parseInt($(this).next().val()) + num;
      
      if (quant <= 0) {
        alert('Số lượng không được = 0');
      }
      else{
        $(this).next().val(quant);
      }
    })
    
    $('.btn-block').on('click','.quantity_increase',function(){
      var num = 0;
      num +=1;
      var quant = parseInt($(this).prev().val()) + num;
      $(this).prev().val(quant);
    })
    
    
    
  </script>
@endpush
@extends('layouts.frontend')
@section('title','Bo suu tap')
@section('content')
	<div class="page page-collection">
		<!-- breadcrumb -->
		<div class="padding-xs">	
			<div class="container">
				<div class="col-lg-12 breadcrumb">
					<ul class="breadcrumb-list">
						<li><i class="fa fa-home"></i> <a href="/">Home</a></li>
						<li>{{$name}}</li>
					</ul>
				</div>
			</div>		
		</div>
		<!-- end breadcrumb -->
		<!-- content -->
		<div class="container">
			<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 sidebar">	
				<div class="row">
					@if ($bestSale)
					<div class="col-xs-12 col-sm-6 col-md-12 col-lg-12">
						<div class="row box-siderbar">
								<h3>Hút khách nhất</h3>			
								@include('frontend.item',['product'=>$bestSale,'width'=>''])
						</div>
					</div>
					@endif
					<!-- END bestseller -->

					@if ($featured)
					<div class="col-xs-12 col-sm-6 col-md-12 col-lg-12">
						<div class="row box-siderbar">
								<h3>Đặc sắc</h3>
								@include('frontend.item',['product'=>$featured,'width'=>''])
						</div>
					</div>
					@endif
					<!-- END special -->
					
					@if ($recommend)
					<div class="col-xs-12 col-sm-6 col-md-12 col-lg-12">
						<div class="row box-siderbar">
								<h3>Nên mua</h3>			
								@include('frontend.item',['product'=>$recommend,'width'=>''])
						</div>
					</div>
					@endif
					<!-- END special -->
				</div>		
			</div>
			<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 collection-content">
				<div class="col-lg-12 searchbar">
                  
                </div>
				
				<!-- FILTER -->
				@include('frontend.product-filter',array('formAction'=> route('search')))
                <!-- PRODUCT GRID -->
                <div class="row product-grid">
                  <?php						
                      if (isset($_GET["view"]) && $_GET["view"] == 'list' ) {
                          $template = 'list';
                          $width = 'col-xs-12 col-sm-6 col-md-12 col-lg-12'; 
                      } else {
                          $template  = 'item';
                          $width = 'col-xs-12 col-sm-6 col-md-4 col-lg-4'; 
                      }
                  ?>
                  @forelse($products as $i => $product)
                      @include('frontend.'.$template,['product'=>$product,'width'=>$width])
                  @empty
                      <div class="alert alert-warning" role="alert">Tạm thời chưa có thông tin tham khảo cho danh mục này!</div>
                  @endforelse				
                </div><!-- END PRODUCT GRID -->
			</div>
		</div>
		<!--end content-->
		@include ('frontend.footer')
	</div>
@endsection
@push('custom-script')
	<script type="text/javascript" src="/packages/ddslick/jquery.ddslick.min.js"></script>
	<script type="text/javascript">
		$(function(){
		  $('#sort-type').ddslick({
		  	width: 160
		  });
		  $('#range').ddslick({
		  	width: 50
		  });
		});
	</script>
@endpush
<div class="row padding-xs">
  <div class="filter">
      <form method="GET" action="{{$formAction}}">
          @if(isset($_GET["view"]))
              @if($_GET["view"] == "list")
                  <div class="button list active disable">
                      <a href="?view=list"><i class="fa fa-th-list"></i></a>
                  </div>
                  <div class="button grid">
                      <a href="?view=grid"><i class="fa fa-th"></i></a>
                  </div>
              @elseif($_GET["view"] == "grid")
                  <div class="button list">
                      <a href="?view=list"><i class="fa fa-th-list"></i></a>
                  </div>
                  <div class="button grid active disable">
                      <a href="?view=grid"><i class="fa fa-th"></i></a>
                  </div>
              @endif
          @else
              <div class="button list">
                  <a href="?view=list"><i class="fa fa-th-list"></i></a>
              </div>
              <div class="button grid active disable">
                  <a href="?view=grid"><i class="fa fa-th"></i></a>
              </div>
          @endif
          <div class="selectbox sort-type">
              <select name="sort" id="sort-type">
                  <option value="1">Mặc định</option>
                  <option value="2">Tên (A-Z)</option>
                  <option value="3">Tên (Z-A)</option>
                  <option value="4">Giá (Thấp - Cao)</option>
                  <option value="5">Giá (Cao - Thấp)</option>
              </select>
          </div>
          <button class="button search">
              <i class="fa fa-search"></i>
          </button>
          <div class="selectbox range">
              <label for="range">số lượng</label>
              <select name="range" id="range">
                  <option value="6">6</option>
                  <option value="15">15</option>
                  <option value="30">30</option>
                  <option value="45">45</option>
                  <option value="90">90</option>
              </select>
          </div>
      </form>
  </div>
</div><!-- END FITLER -->

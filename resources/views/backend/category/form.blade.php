<?php 
  $labels = $category->getLabels();
  $messageBag = $errors->getBag('default');
?>

<div class="x_title">
    <h2>{{$category->name or 'Thêm  danh mục mới'}}</h2>      
    <div class="clearfix"></div>
</div>
<div class="x_content">
    <br>
    <form method="POST" action="{{route($category->isNew() ? 'backend::category.save' : 'backend::category.update')}}"  enctype="multipart/form-data" id="CategoryForm" class="form-horizontal form-label-left">
        @include('common.flash')
        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
        <input type="hidden" name="id" value="{{{ $category->id }}}" />
        
        <div class="form-group {{ $messageBag->has('name') ? 'has-error' : ''}}">
          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Category_name">{{$labels['name']}} <span class="required">*</span>
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" id="Category_name" name="name"  required="required" class="form-control col-md-7 col-xs-12" value="{{$category->name or old('name')}}">
          </div>
        </div>
        <div class="form-group {{ $messageBag->has('slug') ? 'has-error' : ''}}">
          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Category_slug">{{$labels['slug']}} <span class="required">*</span>
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" id="Category_slug" name="slug"  required="required" class="form-control col-md-7 col-xs-12" value="{{ $category->slug or old('slug') }}">
          </div>
        </div>        
        <div class="form-group {{ $messageBag->has('thumbnail') ? 'has-error' : ''}}">
			    <label for="Category_thumbnail" class="control-label col-md-3 col-sm-3 col-xs-12">{{$labels['thumbnail']}} </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <img src="{{$category->getImgBySize(128)}}" width="128">
			    	<input type="file" id="Category_thumbnail" name="thumbnail">
			    	<p class="help-block">Ảnh nhỏ hơn 3MB</p>
            @if(!$category->isNew())
            <input type="checkbox" id="Category_thumbnail_remove" name="thumbnail_remove"><label for="Category_thumbnail_remove">Xóa ảnh hiện tại </label>
            @endif
			    </div>
			  </div>	

        <div class="form-group {{ $messageBag->has('parent_id') ? 'has-error' : ''}}">
          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Category_parent_id">{{$labels['parent_id']}} <span class="required">*</span>
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <select name="parent_id" class="form-control">
                {!!$category->getOptionsCate()!!}
            </select>  
          </div>
        </div>  

        <div class="form-group {{ $messageBag->has('type') ? 'has-error' : ''}}">
          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Category_type">{{$labels['type']}} <span class="required">*</span>
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <select name="type" class="form-control">
                {!!$category->getOptionsType($category->type)!!}
            </select>  
          </div>
        </div>  

        <div class="form-group {{ $messageBag->has('desc') ? 'has-error' : ''}}">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">{{$labels['desc']}}
            </label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <textarea name="desc" class="form-control" rows="3" placeholder="">{{$category->desc or old('desc')}}</textarea>
            </div>
        </div>			  
        <div class="ln_solid"></div>
        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Lưu</button>
            </div>
        </div>
		</form>
</div>

@push('custom-script')
  @parent
  <script type="text/javascript" src="/backend/js/speakingurl.min.js"></script>
  <script type="text/javascript">
  $(document).ready(function(){
    $('#Category_name').keyup(function(){
      $("#Category_slug").val( getSlug($(this).val(), {seperator:'-', lang:'vn'}) )
    })
  })
  </script>
@endpush

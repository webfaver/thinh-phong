@extends('layouts.backend')

@section('title','Quản lý danh mục')

@section('custom-style')
  <link href="{{asset('/backend/css/icheck/flat/green.css')}}" rel="stylesheet">
  <link href="{{asset('/backend/css/datatables/tools/css/dataTables.tableTools.css')}}" rel="stylesheet">
@endsection

<?php 
  $textLabels = App\Models\Category::getLabels();
?>

@section('content')
<div class="">
  <div class="page-title">
    <div class="title_left">
      @include('common.breadcrumb', ['links'=>[],'current'=> 'Quản lý danh mục'])
    </div>

    <div class="title_right">
      <div class="col-md-2 col-sm-2 col-xs-4 form-group pull-right top_search">
        <div class="input-group">
           <a href="{{route('backend::category.add')}}" class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i> Thêm danh mục</a>
        </div>
      </div>
    </div>
  </div>
  <div class="clearfix"></div>

  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>{{$listName}}</h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a href="#"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="{{route('backend::category.home')}}">Danh muc</a>
                </li>
                <li><a href="{{route('backend::category.trash')}}">Thùng rác</a>
                </li>
              </ul>
            </li>
            <li><a href="#"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content" id="tableXContent">
          @if (session('status'))
            <div class="alert alert-success">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <span class="fa fa-check"></span> {{ session('status') }}
            </div>
          @endif
          <table id="example" class="table table-striped responsive-utilities jambo_table">
            <thead>
              <tr class="headings">
                <th class=""><input type="checkbox" class="tableflat" id="checkAll"></th>
                <th class="col-lg-2 col-md-2 col-sm-6 col-xs-12">{{$textLabels['thumbnail']}}</th>
                <th class="col-lg-2 col-md-1 col-sm-6 col-xs-12">{{$textLabels['name']}}</th>
                <th class="col-lg-2 col-md-1 col-sm-6 col-xs-12">{{$textLabels['parent_id']}}</th>
                <th class="col-lg-4 col-md-4 col-sm-6 col-xs-12">{{$textLabels['desc']}}</th>
                <th class="col-lg-2 col-md-4 col-sm-6 col-xs-12 no-link last"><span class="nobr">Tác vụ</span></th>
              </tr>
            </thead>

            <tbody>
            @foreach ($categories as $key => $cate)
              <tr class="{{ $key%2 ? 'odd' : 'even' }} pointer" id="item{{$cate->id}}">
                <td class="a-center ">
                  <input type="checkbox" class="tableflat">
                </td>
                <td class=" "><img src="{{$cate->getImgBySize(128)}}" width="128"></td>
                <td class=" "><a href="{{route('backend::category.edit',[$cate->id])}}">{{$cate->name}}</a></td>
                <td class=" ">{{$cate->parent->name or ''}}</td>
                <td class=" ">{{$cate->desc}}</td>
                <td class=" last">
                  @if ($cate->trashed())
                  <a href="{{route('backend::category.restore', [$cate->id])}}" class="btn btn-primary btn-xs">
                    <i class="fa fa-history"></i> Khôi phục
                  </a>
                  @else
                  <a href="{{route('backend::category.edit', [$cate->id])}}" class="btn btn-primary btn-xs">
                    <i class="fa fa-pencil"></i> Sửa
                  </a>
                  <a onclick="deleteItem('{{route('backend::category.remove')}}',{{$cate->id}})" class="btn btn-danger btn-xs">
                    <i class="fa fa-remove"></i> Xóa
                  </a>
                  @endif
                </td>
              </tr>
            @endforeach                   
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <br>
    <br>
    <br>

  </div>
</div>
@endsection

@push('custom-script')    
  <!-- icheck -->
  <script src="/backend/js/icheck/icheck.min.js"></script>
  <!-- Datatables -->
  <script src="/backend/js/datatables/js/jquery.dataTables.js"></script>
  <script src="/backend/js/datatables/tools/js/dataTables.tableTools.js"></script>
  <script type="text/javascript">
    $(function(){
      setupDataTable('#example');
    });
  </script>

@endpush
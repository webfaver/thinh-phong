@extends('layouts.backend')

@section('title','Thêm danh mục')

@section('content')
	<div class="page-title">
      <div class="title_left">
          @include('common.breadcrumb', ['links'=>[
            [route('backend::category.home'),'Quản lý danh mục'],
          ],'current'=> 'Thêm danh mục'])
      </div>
  </div>
  <div class="clearfix"></div>
  <div class="row">
 		<div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
				@include('backend.category.form') 
			</div>
		</div> 	
  </div>
@endsection
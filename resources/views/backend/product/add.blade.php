@extends('layouts.backend')

@section('title','Thêm tin')

@section('content')
	<div class="page-title">
      <div class="title_left">
        @include('common.breadcrumb', ['links'=>[
          [route('backend::product.home'),'Bảng tin'],
        ],'current'=> 'Thêm tin'])
      </div>
  </div>
  <div class="clearfix"></div>
  <div class="row">
 		<div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
				@include('backend.product.form') 
			</div>
		</div> 	
  </div>
@endsection
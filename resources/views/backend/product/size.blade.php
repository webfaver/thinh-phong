<div class="well add-size">
  <button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <input type="text" name="sizes[]" value="{{$size->name}}" class="form-control col-md-7 col-xs-12 more-size" required="required" placeholder="Tên cỡ, ví dụ 700ml ...">
  <input type="hidden" class="size-id" name="sizes_id[]" value="{{$size->id}}">
  <br>
  <br>
  <input type="text" name="sizes_price[]"  value="{{$size->price}}" class="form-control col-md-7 col-xs-12 more-price" required="required">
</div>    
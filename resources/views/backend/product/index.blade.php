@extends('layouts.backend')

@section('title','Bảng tin')

@push('custom-style')
  <link href="/backend/css/icheck/flat/green.css" rel="stylesheet">
  <link href="/backend/css/datatables/tools/css/dataTables.tableTools.css" rel="stylesheet">
@endpush

<?php 
  $textLabels = App\Models\Product::getLabels();
?>

@section('content')
<div class="">
  <div class="page-title">
    <div class="title_left">
      @include('common.breadcrumb', ['links'=>[],'current'=> 'Bảng tin'])
    </div>

    <div class="title_right">
      <div class="col-md-8 col-sm-8 col-xs-12 form-group pull-right text-right top_search">
        <div class="input-group">          
            <a href="{{route('backend::product.add')}}" class="btn btn-primary"><i class="fa fa-plus"></i> Thêm sản phẩm</a>
        </div>
      </div>
    </div>
  </div>
  <div class="clearfix"></div>

  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Kho hàng</h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a href="#"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="{{route('backend::product.home')}}">Kho hàng</a>
                </li>
                <li><a href="{{route('backend::product.trash')}}">Thùng rác</a>
                </li>
              </ul>
            </li>
            <li><a href="#"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content" id="tableXContent">
          @if (session('status'))
            <div class="alert alert-success">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <span class="fa fa-check"></span> {{ session('status') }}
            </div>
          @endif
          @if (session('error'))
            <div class="alert alert-error">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <span class="fa fa-exclamation-circle"></span> {{ session('error') }}
            </div>
          @endif
          <table id="example" class="table table-striped responsive-utilities jambo_table">
            <thead>
              <tr class="headings">
                <th class=""><input type="checkbox" class="tableflat" id="checkAll"></th>
                <th class="col-md-1 col-sm-6 col-xs-12">{{$textLabels['status']}}</th>
                <th class="col-md-2 col-sm-6 col-xs-12">{{$textLabels['thumbnail']}}</th>
                <th class="col-md-2 col-sm-6 col-xs-12">{{$textLabels['name']}}</th>
                <th class="col-md-2 col-sm-6 col-xs-12">{{$textLabels['brand']}}</th>
                <th class="col-md-2 col-sm-6 col-xs-12">{{$textLabels['price']}}</th>
                <th class="col-md-1 col-sm-6 col-xs-12">{{$textLabels['updated_at']}}</th>
                <th class="col-md-2 col-sm-6 col-xs-12 no-link last"><span class="nobr">Tác vụ</span></th>
              </tr>
            </thead>

            <tbody>
            @foreach ($products as $key => $product)
              <tr class="{{ $key%2 ? 'odd' : 'even' }} pointer" id="item{{$product->id}}">
                <td class="a-center ">
                  <input type="checkbox" class="tableflat">
                </td>
                <td class=" ">{!!$product->getStatusLabel()!!}</td>
                <td class=" "><a href="{{route('backend::product.edit',[$product->id])}}"><img src="{{$product->getImgBySize(128)}}" width="128"></a></td>
                <td class=" ">
                  <strong><a href="{{route('backend::product.edit',[$product->id])}}">{{$product->name}}</a></strong>
                </td>
                <td class=" ">{{$product->brand}}</td>
                <td class=" ">{{$product->price}}</td>
                <td class=" ">{{$product->updated_at}}</td>
                <td class=" last">
                  @if ($product->trashed())
                      <a href="{{route('backend::product.restore', [$product->id])}}" class="btn btn-primary btn-xs">
                        <i class="fa fa-history"></i> Khôi phục
                      </a>
                  @else
                      <a href="{{route('backend::product.edit',[$product->id])}}" class="btn btn-primary btn-xs">
                        <i class="fa fa-pencil"></i> Sửa
                      </a>
                      <a onclick="deleteItem('{{route('backend::product.remove')}}',{{$product->id}})" class="btn btn-danger btn-xs">
                        <i class="fa fa-remove"></i> Xóa
                      </a>
                  @endif
                </td>
              </tr>
            @endforeach                   
            </tbody>
          </table>
          @include('common.paging', ['data'=>$products])
        </div>
      </div>
    </div>
    <br>
    <br>
    <br>

  </div>
</div>
@endsection

@push('custom-script')    
  <!-- icheck -->
  <script src="{{asset('/backend/js/icheck/icheck.min.js')}}"></script>
  <!-- Datatables -->
  <script src="{{asset('/backend/js/datatables/js/jquery.dataTables.js')}}"></script>
  <script>
    $(function(){
      setupDataTable('#example', {
        "bPaginate": false,
        "bInfo" : false,
        'iDisplayLength': -1
      });
    });
  </script>
@endpush

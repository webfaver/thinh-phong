@extends('layouts.backend')

@section('title','Chỉnh sửa')

@section('content')
	<div class="page-title">
      <div class="title_left">
        @include('common.breadcrumb', ['links'=>[
          [route('backend::product.home'),'Kho hàng'],
        ],'current'=> $product->title])
      </div>
      <div class="title_right">
          <div class="col-md-8 col-sm-8 col-xs-12 form-group pull-right text-right top_search">
                <div class="input-group">
                  <a href="{{route('backend::product.add')}}" class="btn btn-primary"><i class="fa fa-plus"></i> Thêm sản phẩm</a>
                </div>
            </div>
      </div>
  </div>
  <div class="clearfix"></div>
  <div class="row">
 		<div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
				@include('backend.product.form') 
			</div>
		</div> 	
  </div>
  @include('backend.gallery.album',['objType'=>'product','objId'=>$product->id,'images'=>$product->images()])
@endsection
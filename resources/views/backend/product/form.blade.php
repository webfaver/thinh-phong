@push('custom-style')
  <link rel="stylesheet" href="{{asset('/backend/css/normalize.css')}}" />
  <link rel="stylesheet" href="{{asset('/backend/css/ion.rangeSlider.css')}}" />
  <link rel="stylesheet" href="{{asset('/backend/css/ion.rangeSlider.skinFlat.css')}}" />
  <style type="text/css">
    .irs-slider {
        width: 18px;
        height: 18px;
        top: 22px;
        background-position: 0 -119px;
        background-color: #DA4453;
        border-radius: 9px;
    }
  </style>
@endpush
<?php
  $labels = $product->getLabels();
  $messageBag = $errors->getBag('default');
?>
<div class="x_title">
    <h2>{{$product->name or 'Thêm tin mới'}}</h2>      
    <div class="clearfix"></div>
</div>
<div class="x_content">
    <br>		
    <form method="POST" action="{{route($product->isNew() ? 'backend::product.save' : 'backend::product.update')}}"  enctype="multipart/form-data" id="ProductForm" class="form-horizontal form-label-left">
        @include('common.flash')

        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        <input type="hidden" name="id" value="{{ $product->id }}" />        

        <div class="form-group {{ $messageBag->has('name') ? 'has-error' : ''}}">
          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Product_name">{{$labels['name']}} <span class="required">*</span>
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" id="Product_name" name="name" required="required" class="form-control col-md-7 col-xs-12" value="{{$product->name or old('name')}}">
          </div>
        </div>

        <div class="form-group {{ $messageBag->has('slug') ? 'has-error' : ''}}">
          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Product_slug">{{$labels['slug']}} <span class="required">*</span>
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" id="Product_slug" name="slug"  required="required" class="form-control col-md-7 col-xs-12" value="{{ $product->slug or old('slug') }}">
          </div>
        </div>        	

        <div class="form-group {{ $messageBag->has('brand') ? 'has-error' : ''}}">
          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Product_brand">{{$labels['brand']}}
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" id="Product_brand" name="brand" class="form-control col-md-7 col-xs-12" value="{{$product->brand or old('brand')}}">
          </div>
        </div>

        <div class="form-group {{ $messageBag->has('plu') ? 'has-error' : ''}}">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">{{$labels['plu']}}
            </label>
            <div class="col-md-9 col-sm-9 col-xs-12">
              <input type="text" id="Product_plu" name="plu"  class="form-control col-md-7 col-xs-12" value="{{ $product->plu or old('plu') }}">
            </div>
        </div> 

        <div class="form-group {{ $messageBag->has('desc') ? 'has-error' : ''}}">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">{{$labels['desc']}}
            </label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <textarea id="Product_desc" name="desc" class="form-control" rows="3" placeholder="">{{$product->desc or old('desc')}}</textarea>
            </div>
        </div>

        <div class="form-group {{ $messageBag->has('category_id') ? 'has-error' : ''}}">
          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Product_category_id">{{$labels['category_id']}} <span class="required">*</span>
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <select id="Product_category_id" name="category_id" class="form-control">
                {!!$product->getCategoryOptions()!!}
            </select>  
          </div>
        </div>
  
        <div class="form-group {{ $messageBag->has('price') ? 'has-error' : ''}}">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">{{$labels['price']}}
            </label>
            <div class="col-md-9 col-sm-9 col-xs-12">
              <input type="text" id="Product_price" name="price"  required="required" class="form-control col-md-7 col-xs-12" value="{{ $product->price or old('price') }}">
            </div>
        </div>          

        <div class="form-group {{ $messageBag->has('sizes') ? 'has-error' : ''}}">
          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Product_quantity">Sizes
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            @forelse($product->sizes as $size)
              @include('backend.product.size')
            @empty
              <?php $size = new App\Models\ProductSize();?>
              @include('backend.product.size')
            @endforelse 
            <a href="javascript:void(0);" id="addSize" class="btn btn-default">Thêm size</a>
          </div>
        </div>  

        <div class="form-group {{ $messageBag->has('attr') ? 'has-error' : ''}}">
          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Product_attr">{{$labels['attr']}}
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            {!!$product->getAttrOptions()!!}
          </div>
        </div>              

        <div class="ln_solid"></div>
        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Lưu</button>
                    
                    @if ($product->status == App\Models\Product::STT_SUSPEND)
                      <a href="{{route('backend::product.online',[$product->id])}}?_token={{csrf_token()}}" class="btn btn-success"><i class="fa fa-rocket"></i> Đăng bán</a>
                    @elseif($product->status == App\Models\Product::STT_ONLINE)
                      <a href="{{route('backend::product.suspend',[$product->id])}}?_token={{csrf_token()}}" class="btn btn-warning"><i class="fa fa-lock"></i> Tạm ngưng bán</a>
                    @endif
            </div>
        </div>

		</form>
</div>

@push('custom-script')
  <!-- JS PARAMETER -->
  <input type="hidden" id="" value="">
  
  <!-- ADVANCED SCRIPT -->
  <script type="text/javascript" src="{{asset('/backend/packages/ckeditor/ckeditor.js')}}"></script>
  <script type="text/javascript" src="{{asset('/backend/packages/ckeditor/config.js')}}"></script>
  <script type="text/javascript" src="{{asset('/backend/js/speakingurl.min.js')}}"></script>
  <!-- ionRangeSlider -->
  <script type="text/javascript" src="{{asset('/backend/js/ion_range/ion.rangeSlider.min.js')}}"></script>
  <!-- iCheck js -->
  <script src="{{asset('/backend/js/icheck/icheck.min.js')}}"></script>
  <!-- Custom script -->
  <script type="text/javascript">
    $(document).ready(function () {
      CKEDITOR.replace('Product_desc')

      
      $('#Product_name').keyup(function(){
        $("#Product_slug").val( getSlug($(this).val(), {seperator:'-', lang:'vn'}) )
      });        
         
      $("#Product_price").ionRangeSlider({
        min:0,
        max: 50000000,
        from: $('#Product_price').val()  || 0,
        step: 100000,
        grid: true,
        keyboard:true,
        keyboard_step: 0.1,
        postfix: " &#8363;"
      });

      $('input.more-price').each(function(index, ele){
        $(this).ionRangeSlider({
          min:0,
          max: 10000000,
          from: $(this).val()  || 50000,
          step: 50000,
          grid: true,
          keyboard:true,
          keyboard_step: 0.05,
          postfix: " &#8363;"
        }); 
      })

      $('#addSize').click(addSize);
      $('button.close').click(function(e){
        if ($('div.add-size').length > 1) {          
          $(this).parent().slideUp('slow', function(){
            $(this).remove();
          });
        } else {
          $(this).next().val('');
        }
      });

      $('input.flat').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass: 'iradio_flat-green'
      });
    });

    function addSize(){
        $tmp = $(this).prev();
        $slider = $tmp.find('input.more-price').data('ionRangeSlider');
        $slider && $slider.destroy();
        $new = $tmp.clone();
        $inputOld = $tmp.find('input.more-price');        
        $inputNew = $new.find('input.more-price');        
        addBonusPrice($inputOld, $inputOld.val());
        addBonusPrice($inputNew, $inputNew.val());
        $new.find('input.more-size').val('');
        $new.find('input.size-id').val('');
        $(this).before($new);
      }
      function addBonusPrice(input, initPrice) {
        input.ionRangeSlider({
          min:0,
          max: 10000000,
          from: initPrice,
          step: 50000,
          grid: true,
          keyboard:true,
          keyboard_step: 0.05,
          postfix: " &#8363;"
        });
      }
  </script>
@endpush

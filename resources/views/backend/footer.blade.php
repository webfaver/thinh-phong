<footer>
    <div class="">
        <p class="pull-right">Power by <a href="http://webfaver.com/work" target="_blank">WebFaver.Com</a> 
            <span class="lead">| © {{date('Y')}} <i class="fa fa-glass"></i> {{env('APP_NAME','Cherry CMS')}}</span>
        </p>
    </div>
    <div class="clearfix"></div>
</footer>
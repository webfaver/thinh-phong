@extends('layouts.backend')

@section('title','Đơn hàng #'.$order->id.': '.$order->name.' - '. $order->phone)

@section('content')
<?php 
  $labels = $order->getLabels();
?>

<div class="">
  <div class="page-title">
    <div class="title_left">
      <h3>Chi tiết đơn hàng</h3>
    </div>
  </div>
  <div class="clearfix"></div>

  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel" style="min-height:600px;">
        <div class="x_title">
          <h2>Đơn hàng #{{$order->id}}: {{$order->name}} - {{$order->phone}}</h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x-content">
          @include('common.flash')
          <div class="row">
            <strong class="col-md-1 col-sm-6 col-xs-12">{{$labels['name']}}:</strong>
            <p class="col-md-6 col-sm-6 col-xs-12">{{ $order->name }}</p>
          </div>
          <div class="row">
            <strong class="col-md-1 col-sm-6 col-xs-12">{{$labels['phone']}}:</strong>
            <p class="col-md-6 col-sm-6 col-xs-12">{{ $order->phone }}</p>
          </div>
          <div class="row">
            <strong class="col-md-1 col-sm-6 col-xs-12">{{$labels['email']}}:</strong>
            <p class="col-md-6 col-sm-6 col-xs-12">{{ $order->email }}</p>
          </div>
          <div class="row">
            <strong class="col-md-1 col-sm-6 col-xs-12">{{$labels['address']}}:</strong>
            <p class="col-md-6 col-sm-6 col-xs-12">{{ $order->address }}</p>
          </div>                   
          <div class="row">
            <strong class="col-md-1 col-sm-12 col-xs-12">{{$labels['created_at']}}:</strong>
            <p class="col-md-6 col-sm-6 col-xs-12">{{date_create($order->created_at)->format('d/m/Y H:i:s') }}</p>
          </div>
          <table class="table table-bordered">
            <tr>
              <th>#</th>
              <th>Tên sản phẩm</th>
              <th>Cỡ</th>
              <th>Số lượng</th>
            </tr>
            @foreach($order->details as $i => $detail)
              <tr>
                <td>{{$i+1}}</td>
                <td><a href="{{route('productDetail',[$detail->product->slug])}}" target="_blank">{{$detail->product->name}}</a></td>
                <td>{{$detail->size->name}}</td>
                <td>{{$detail->quantity}}</td>
              </tr>
            @endforeach
          </table>
          <div class="row">
            <button onclick="deleteItem('{{route('backend::order.remove')}}',{{$order->id}}, '#item', 'div.x-content')" type="button" class="btn btn-danger"> Xóa </button>
            @if($order->status == App\Models\Order::STT_NEW)
            <a href="{{route('backend::order.done',[$order->id])}}?_token={{csrf_token()}}" class="btn btn-success">Đánh dấu đã xử lý đơn hàng xong</a>
            @elseif($order->status == App\Models\Order::STT_DONE)
            <a href="{{route('backend::order.none',[$order->id])}}?_token={{csrf_token()}}" class="btn btn-warning"> Chưa xử lý xong</a>
            @endif
          </div>
        </div> <!-- END X-CONTENT -->
      </div>
    </div>
  </div>
</div>
@endsection
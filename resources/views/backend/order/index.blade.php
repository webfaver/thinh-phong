@extends('layouts.backend')

@section('title','Danh sách cấu hình')

@section('custom-style')
    <link href="{{asset('/backend/css/icheck/flat/green.css')}}" rel="stylesheet">
    <link href="{{asset('/backend/css/datatables/tools/css/dataTables.tableTools.css')}}" rel="stylesheet">
@endsection

<?php 
    $textLabels = App\Models\Order::getLabels();
?>

@section('content')
<div class="">
    <div class="page-title">
        <div class="title_left">
            <div class="title_left">
                @include('common.breadcrumb', ['links'=>[],'current'=> 'Danh sách đơn hàng'])
            </div>
            <div class="clearfix"></div>            
        </div>

        <div class="title_right">
            <div class="col-md-2 col-sm-2 col-xs-4 form-group pull-right top_search">
                
            </div>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Bảng đơn hàng</h2>
                    <ul class="nav navbar-right panel_toolbox">
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content" id="tableXContent">
                    @include('common.flash')
                    <table id="example" class="table table-striped responsive-utilities jambo_table">
                        <thead>
                            <tr class="headings">
                                <th class="col-md-1 col-sm-6 col-xs-12"><input type="checkbox" class="tableflat" id="checkAll"></th>
                                <th class="col-md-2 col-sm-6 col-xs-12">{{$textLabels['name']}}</th>
                                <th class="col-md-1 col-sm-6 col-xs-12">{{$textLabels['phone']}}</th>
                                <th class="col-md-2 col-sm-6 col-xs-12">{{$textLabels['email']}}</th>
                                <th class="col-md-4 col-sm-6 col-xs-12">{{$textLabels['address']}}</th>
                                <th class="col-md-2 col-sm-6 col-xs-12 no-link last"><span class="nobr">Tác vụ</span></th>
                            </tr>
                        </thead>

                        <tbody>
                        @foreach ($orders as $key => $order)
                            <tr class="{{ $key%2 ? 'odd' : 'even' }} pointer" id="item{{$order->id}}">
                                <td class="a-center ">
                                    <input type="checkbox" class="tableflat">
                                    {!!$order->getStatusLabel()!!}
                                </td>                                
                                <td class=" "><a href="{{route('backend::order.show',[$order->id])}}">{{$order->name}}</a></td>
                                <td class=" ">{{$order->phone}}</td>
                                <td class=" ">{{$order->email}}</td>
                                <td class=" ">{{$order->address}}</td>
                                <td class=" last">
                                    <a href="{{route('backend::order.show', [$order->id])}}" class="btn btn-primary btn-xs">
                                        <i class="fa fa-pencil"></i> Sửa
                                    </a>
                                    <a onclick="deleteItem('{{route('backend::order.remove')}}',{{$order->id}})" class="btn btn-danger btn-xs">
                                        <i class="fa fa-remove"></i> Xóa
                                    </a>
                                </td>
                            </tr>
                        @endforeach                   
                        </tbody>
                    </table>
                    @include('common.paging', ['data'=>$orders])
                </div>
            </div>
        </div>
        <br>
        <br>
        <br>

    </div>
</div>
@endsection

@section('custom-script')    
    <!-- icheck -->
    <script src="/backend/js/icheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="/backend/js/datatables/js/jquery.dataTables.js"></script>
    <script src="/backend/js/datatables/tools/js/dataTables.tableTools.js"></script>
    <script type="text/javascript">
    $(function(){
      setupDataTable('#example', {
        "bPaginate": false,
        "bInfo" : false,
        'iDisplayLength': -1
      });
    });
    </script>
@endsection
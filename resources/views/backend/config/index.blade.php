@extends('layouts.backend')

@section('title','Danh sách cấu hình')

@section('custom-style')
    <link href="/backend/css/icheck/flat/green.css" rel="stylesheet">
    <link href="/backend/css/datatables/tools/css/dataTables.tableTools.css" rel="stylesheet">
@endsection

<?php 
    $textLabels = App\Models\Config::getLabels();
?>

@section('content')
<div class="">
    <div class="page-title">
        <div class="title_left">
            <div class="title_left">
                @include('common.breadcrumb', ['links'=>[],'current'=> 'Danh sách cấu hình'])
            </div>
            <div class="clearfix"></div>
            <div class="alert alert-warning" role="alert">
                <strong>Cảnh báo:</strong>
                <ul>
                    <li>Chỉ dành cho Quản trị viên. Hãy cẩn thận với cấu hình.</li>
                    <li>Thay đổi của bạn có thể ảnh hưởng đến trang web sau {{env('CACHE_TIME')}} phút.</li>
                </ul>
            </div>
        </div>

        <div class="title_right">
            <div class="col-md-2 col-sm-2 col-xs-4 form-group pull-right top_search">
                
            </div>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Bảng cấu hình</h2>
                    <ul class="nav navbar-right panel_toolbox">
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content" id="tableXContent">
                    @if (session('status'))
                        <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <span class="fa fa-check"></span> {{ session('status') }}
                        </div>
                    @endif
                    <table id="example" class="table table-striped responsive-utilities jambo_table">
                        <thead>
                            <tr class="headings">
                                <th class="col-md-1 col-sm-6 col-xs-12"><input type="checkbox" class="tableflat" id="checkAll"></th>
                                <th class="col-md-2 col-sm-6 col-xs-12">{{$textLabels['key']}}</th>
                                <th class="col-md-2 col-sm-6 col-xs-12">{{$textLabels['value']}}</th>
                                <th class="col-md-4 col-sm-6 col-xs-12">{{$textLabels['description']}}</th>
                                <th class="col-md-2 col-sm-6 col-xs-12 no-link last"><span class="nobr">Tác vụ</span></th>
                            </tr>
                        </thead>

                        <tbody>
                        @foreach ($configs as $key => $config)
                            <tr class="{{ $key%2 ? 'odd' : 'even' }} pointer" id="item{{$config->id}}">
                                <td class="a-center ">
                                    <input type="checkbox" class="tableflat">
                                </td>                                
                                <td class=" "><a href="{{route('backend::config.edit',[$config->id])}}">{{$config->key}}</a></td>
                                <td class=" ">{{ mb_substr($config->value, 0, 64) }}</td>
                                <td class=" ">{{$config->description}}</td>
                                <td class=" last">
                                    <a href="{{route('backend::config.edit', [$config->id])}}" class="btn btn-primary btn-xs">
                                        <i class="fa fa-pencil"></i> Sửa
                                    </a>
                                    <a onclick="deleteItem('{{route('backend::config.remove')}}',{{$config->id}})" class="btn btn-danger btn-xs">
                                        <i class="fa fa-remove"></i> Xóa
                                    </a>
                                </td>
                            </tr>
                        @endforeach                   
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <br>
        <br>
        <br>

    </div>
</div>
@endsection

@section('custom-script')    
    <!-- icheck -->
    <script src="/backend/js/icheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="/backend/js/datatables/js/jquery.dataTables.js"></script>
    <script src="/backend/js/datatables/tools/js/dataTables.tableTools.js"></script>
    <script type="text/javascript">
    $(function(){
        setupDataTable('#example');
    });
    </script>
@endsection
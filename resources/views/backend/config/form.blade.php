<?php 
    $labels = $config->getLabels();
    $messageBag = $errors->getBag('default');
?>
<div class="x_title">
    <h2>{{$config->key or 'Thêm cấu hình'}}</h2>      
    <div class="clearfix"></div>
</div>
<div class="x_content">
    <br>		
    <form method="POST" action="{{route($config->isNew() ? 'backend::config.save' : 'backend::config.update')}}"  enctype="multipart/form-data" id="ConfigForm" class="form-horizontal form-label-left">
        @include('common.flash')

        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        <input type="hidden" name="id" value="{{ $config->id }}" />        

        <div class="form-group {{ $messageBag->has('key') ? 'has-error' : ''}}">
          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Config_key">{{$labels['key']}} <span class="required">*</span>
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" id="Config_name" name="key" required="required" class="form-control col-md-7 col-xs-12" value="{{$config->key or old('key')}}" {{ !$config->isNew() ? 'readonly="readonly"' : ''}}>
          </div>
        </div>   	

        <div class="form-group {{ $messageBag->has('value') ? 'has-error' : ''}}">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">{{$labels['value']}} <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" id="Config_value" name="value" class="form-control" rows="3" placeholder="" value="{{$config->value or old('value')}}">
                <a href="" class="btn btn-primary popup_selector" data-inputid="Config_value">Chọn ảnh nếu cần...</a>
            </div>
        </div> 

        <div class="form-group {{ $messageBag->has('description') ? 'has-error' : ''}}">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">{{$labels['description']}}
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <textarea id="Config_description" name="description" class="form-control" rows="3" placeholder="">{{$config->description or old('description')}}</textarea>
            </div>
        </div>

        <div class="ln_solid"></div>
        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                <button type="reset" class="btn btn-primary">Hủy</button>
                <button type="submit" class="btn btn-success">Lưu</button>
            </div>
        </div>

		</form>
</div>
@push('custom-style')
    <link href="{{asset('/packages/jquery-colorbox/example1/colorbox.css')}}" rel="stylesheet">    
@endpush
@push('custom-script')
    <script type="text/javascript" src="{{asset('/packages/jquery-colorbox/jquery.colorbox-min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/packages/barryvdh/elfinder/js/standalonepopup.min.js')}}"></script>
@endpush
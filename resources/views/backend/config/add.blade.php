@extends('layouts.backend')

@section('title','Thêm cấu hình')

@section('content')
	<div class="page-title">
      <div class="title_left">
          <h3>Thêm cấu hình</h3>
      </div>
  </div>
  <div class="clearfix"></div>
  <div class="row">
 		<div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
				@include('backend.config.form') 
			</div>
		</div> 	
  </div>
@endsection
@extends('layouts.backend')

@section('title','Sửa cấu hình')

@section('content')
  <div class="page-title">
      <div class="title_left">
          @include('common.breadcrumb', ['links'=>[
            [route('backend::config.home'),'Danh sách cấu hình'],
          ],'current'=> 'Sửa cấu hình'])
          <div class="alert alert-warning" role="alert">
          <strong>Cảnh báo:</strong>
          <br/>Hết sức cẩn thận khi thay đổi cấu hình. 
          <br/>Đừng thay đổi nếu không biết.
          </div>
      </div>
      <div class="title_right">
          <div class="col-md-2 col-sm-2 col-xs-4 form-group pull-right top_search">
              <div class="input-group">
              </div>
          </div>
      </div>
  </div>
  <div class="clearfix"></div>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        @include('backend.config.form') 
      </div>
    </div>  
  </div>
@endsection
<?php
    /**
     * These are parameters for view
     * @param $objType type/table of model
     * @param $objId id of main model
     * @param $images Image collection
     */
?>
@push('custom-style')
<style type="text/css">
    .drupload {
        border: 1px solid #ddd;
        padding: 20px 10px;
    }
    input.drupload_files[type="file"] {
        display: none;
    }
    .thumbnail.add-item {
        border: 6px dashed #ddd;
        position: relative;
        cursor: pointer;
    }
    .thumbnail .image img {
        width: 100%; 
        height: 100%;
        object-fit: cover;
        display: block;
    } 
    .thumbnail .item-title, .thumbnail .item-content {
        width: 100%;
        word-wrap: break-word;
    }
    .add-item .add-icon {
        font-size: 6em;
        color: #ddd;
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
    }
    .is-dragover {
        background: #ddd;
    }
    .item-template {
        display: none;
    }
    .item.is-uploading .view-first .mask {
        opacity: 1;
        height: 100%;
        width: 0%;
        background: rgba(49, 208, 0, 0.5);
    }
    .item.is-error .view-first .mask {
        opacity: 1;
        height: 100%;
        width: 100%;
        background: rgba(200, 50, 50, 0.5);
    }
    .item.is-error .view-first p {
        opacity: 1;
        transform: translateY(0px);
    }
    .thumbnail .caption {
        height: 100%;
    }
</style>
@endpush
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Danh sách ảnh</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>                    
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

                <p>Kéo thả nhiều file một lúc vào dấu cộng để upload</p>
                <div class="row drupload">
                    <div class="col-md-55 item item-template">
                        <div class="thumbnail">
                            <div class="image view view-first">
                                <img src="" alt="image" />
                                <div class="mask">
                                    <p>Edit your photo</p>
                                    <div class="tools tools-bottom">
                                        <a class="item-original" href="" target="_blank"><i class="fa fa-link"></i></a>
                                        <a class="item-edit" href="#"><i class="fa fa-pencil"></i></a>
                                        <a class="item-delete" href="#"><i class="fa fa-times"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="caption">
                                <p><strong class="item-title"></strong></p>
                                <p class="item-content"></p>
                            </div>
                        </div>
                        <input type="hidden" class="hidden-data" value="">
                    </div>

                    @foreach($images as $img)
                        <div class="col-md-55 item" remote-id="{{$img->id}}">
                            <div class="thumbnail">
                                <div class="image view view-first">
                                    <img src="{{asset($img->getThumbBySize('256'))}}" alt="image" />
                                    <div class="mask">
                                        <p>Edit your photo</p>
                                        <div class="tools tools-bottom">
                                            <a class="item-original" href="{{asset($img->url)}}" target="_blank"><i class="fa fa-link"></i></a>
                                            <a class="item-edit" href="#"><i class="fa fa-pencil"></i></a>
                                            <a class="item-delete" href="#"><i class="fa fa-times"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="caption">
                                    <p><strong class="item-title">{{$img->title}}</strong></p>
                                    <p class="item-content">{{$img->desc}}</p>
                                </div>
                            </div>
                            <input type="hidden" class="hidden-data" value="{{json_encode($img->toArray())}}">
                        </div>
                    @endforeach
                    
                    <input type="file" class="drupload_files" multiple>
                    <div class="col-md-55">
                        <div class="thumbnail add-item">
                            <i class="fa fa-plus add-icon"></i>
                            <input type="hidden" class="drupload_action" value="{{route('backend::image.upload')}}">
                            <input type="hidden" class="drupload_delete" value="{{route('backend::image.deleteItem')}}">
                            <input type="hidden" class="drupload_update" value="{{route('backend::image.updateItem')}}">
                            <input type="hidden" id="obj_type" value="{{$objType}}">
                            <input type="hidden" id="obj_id" value="{{$objId}}">
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade gallery-item-detail" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                <div class="message">
                    
                </div>
                <form id="galleryItemDetail" action="{{route('backend::image.updateItem')}}" method="POST"  class="form-horizontal form-label-left">
                    <input type="hidden" name="item_id" val="">
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Ảnh
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <img src="" alt="" style="width: 100%;">
                        </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Gallery_Item_Title">Tên <span class="required">*</span>
                      </label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
                        <input type="text" id="Gallery_Item_title" name="item_title"  required="required" class="form-control col-md-7 col-xs-12" value="">
                      </div>
                    </div>          
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Gallery_Item_link">Link
                      </label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
                        <input type="text" id="Gallery_Item_link" name="item_link"  required="required" class="form-control col-md-7 col-xs-12" value="" placeholder="(Không bắt buộc) http://example.com/news/hello-world.html">
                      </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Nội dung
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <textarea id="Gallery_Item_content" name="item_content" class="form-control" rows="3" placeholder=""></textarea>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                <button type="button" class="btn btn-primary" id="updateItemDetail">Lưu thay đổi</button>
            </div>

        </div>
    </div>
</div>
@push('custom-script')
<script type="text/javascript" src="{{asset('/backend/packages/nhtua/drupload/drupload.js')}}"></script>
@endpush
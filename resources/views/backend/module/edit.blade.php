@extends('layouts.backend')

@section('title','Sửa cấu hình')

@section('content')
  <div class="page-title">
      <div class="title_left">
          @include('common.breadcrumb', ['links'=>[
            [route('backend::module.home'),'Danh sách module'],
          ],'current'=> 'Sửa module'])
      </div>
      <div class="title_right">
          <div class="col-md-2 col-sm-2 col-xs-4 form-group pull-right top_search">
              <div class="input-group">
                 <a href="{{route('backend::module.add')}}" class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i> Thêm module</a>
              </div>
          </div>
      </div>
  </div>
  <div class="clearfix"></div>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        @include('backend.module.form') 
      </div>
    </div>  
  </div>
  @include('backend.module.type.item-picker')
@endsection
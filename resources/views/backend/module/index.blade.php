@extends('layouts.backend')

@section('title','Danh sách module')

@section('custom-style')
  <link href="{{asset('/backend/css/icheck/flat/green.css')}}" rel="stylesheet">
  <link href="{{asset('/backend/css/datatables/tools/css/dataTables.tableTools.css')}}" rel="stylesheet">
@endsection

<?php 
  $textLabels = App\Models\Module::getLabels();
?>

@section('content')
<div class="">
  <div class="page-title">
    <div class="title_left">
      <div class="title_left">
        @include('common.breadcrumb', ['links'=>[],'current'=> 'Danh sách module'])
      </div>
      <div class="clearfix"></div>            
    </div>

    <div class="title_right">
      <div class="col-md-2 col-sm-2 col-xs-4 form-group pull-right top_search">
        <div class="input-group">
           <a href="{{route('backend::module.add')}}" class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i> Thêm module</a>
        </div>
      </div>
    </div>
  </div>
  <div class="clearfix"></div>

  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Danh sách module</h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a href="#"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="{{route('backend::module.home')}}">Danh muc</a>
                </li>
                <li><a href="{{route('backend::module.trash')}}">Thùng rác</a>
                </li>
              </ul>
            </li>
            <li><a href="#"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content" id="tableXContent">
          @include('common.flash')
          <table id="example" class="table table-striped responsive-utilities jambo_table">
            <thead>
              <tr class="headings">
                <th class="col-md-1 col-sm-6 col-xs-12"><input type="checkbox" class="tableflat" id="checkAll"></th>
                <th class="col-md-1 col-sm-6 col-xs-12">{{$textLabels['id']}}</th>
                <th class="col-md-2 col-sm-6 col-xs-12">{{$textLabels['type']}}</th>
                <th class="col-md-2 col-sm-6 col-xs-12">{{$textLabels['name']}}</th>
                <th class="col-md-4 col-sm-6 col-xs-12">{{$textLabels['desc']}}</th>
                <th class="col-md-2 col-sm-6 col-xs-12 no-link last"><span class="nobr">Tác vụ</span></th>
              </tr>
            </thead>

            <tbody>
            @foreach ($modules as $key => $module)
              <tr class="{{ $key%2 ? 'odd' : 'even' }} pointer" id="item{{$module->id}}">
                <td class="a-center ">
                  <input type="checkbox" class="tableflat">
                </td>  
                <td class=" ">{{$module->id}}</td>                              
                <td class=" ">{{$module->getTypeName()}}</td>
                <td class=" "><a href="{{route('backend::module.edit',[$module->id])}}">{{$module->name}}</a></td>
                <td class=" ">{{$module->desc}}</td>
                <td class=" last">
                  <a href="{{route('backend::module.edit', [$module->id])}}" class="btn btn-primary btn-xs">
                    <i class="fa fa-pencil"></i> Sửa
                  </a>
                  <a onclick="deleteItem('{{route('backend::module.remove')}}',{{$module->id}})" class="btn btn-danger btn-xs">
                    <i class="fa fa-remove"></i> Xóa
                  </a>
                </td>
              </tr>
            @endforeach                   
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <br>
    <br>
    <br>
  </div>
</div>
@endsection

@section('custom-script')    
  <!-- icheck -->
  <script src="{{asset('/backend/js/icheck/icheck.min.js')}}"></script>
  <!-- Datatables -->
  <script src="{{asset('/backend/js/datatables/js/jquery.dataTables.js')}}"></script>
  <script src="{{asset('/backend/js/datatables/tools/js/dataTables.tableTools.js')}}"></script>
  <script type="text/javascript">
  $(function(){
    setupDataTable('#example');
  });
  </script>
@endsection
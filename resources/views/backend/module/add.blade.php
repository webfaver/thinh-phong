@extends('layouts.backend')

@section('title','Thêm module')

@section('content')
	<div class="page-title">
      <div class="title_left">
          <h3>Thêm module</h3>
      </div>
  </div>
  <div class="clearfix"></div>
  <div class="row">
 		<div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
				@include('backend.module.form') 
			</div>
		</div> 	
  </div>
@endsection
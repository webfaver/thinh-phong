<?php 
  $labels = $module->getLabels();
  $messageBag = $errors->getBag('default');
?>
<div class="x_title">
  <h2>{{$module->name or 'Thêm module'}}</h2>      
  <div class="clearfix"></div>
</div>
<div class="x_content">
  <br>    
  <form method="POST" action="{{route($module->isNew() ? 'backend::module.save' : 'backend::module.update')}}"  enctype="multipart/form-data" id="ModuleForm" class="form-horizontal form-label-left">
    @include('common.flash')

    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
    <input type="hidden" name="id" value="{{ $module->id }}" />        

    <div class="form-group {{ $messageBag->has('name') ? 'has-error' : ''}}">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Module_name">{{$labels['name']}} <span class="required">*</span>
      </label>
      <div class="col-md-6 col-sm-6 col-xs-12">
      <input type="text" id="Module_name" name="name" required="required" class="form-control col-md-7 col-xs-12" value="{{$module->name or old('name')}}">
      </div>
    </div>    
    <div class="form-group {{ $messageBag->has('desc') ? 'has-error' : ''}}">
      <label class="control-label col-md-3 col-sm-3 col-xs-12">{{$labels['desc']}}
      </label>
      <div class="col-md-6 col-sm-6 col-xs-12">
        <textarea id="Module_desc" name="desc" class="form-control" rows="3" placeholder="">{{$module->desc or old('desc')}}</textarea>
      </div>
    </div>

    <div class="form-group {{ $messageBag->has('type') ? 'has-error' : ''}}">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Module_type">{{$labels['type']}} <span class="required">*</span>
      </label>
      <div class="col-md-6 col-sm-6 col-xs-12">
      <select name="type" class="form-control">
        {!!$module->getOptionsType()!!}
      </select>  
      </div>
    </div>  


    <div class="ln_solid"></div>
    <div class="form-group">
      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
        <button type="submit" class="btn btn-success"><i class="fa fa-floppy-o"></i> Lưu</button>
      </div>
    </div>

    </form>
</div>
@push('custom-style')
  
@endpush
@push('custom-script')

@endpush
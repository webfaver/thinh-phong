@push('custom-style')
<style type="text/css">
  a.btn {
    margin-bottom: 0px;
  }  
  select[multiple] {
    min-height: 300px;
  }
  .col-md-2.text-center.column-buttons {
    padding-top: 24px;
  }
  .column-buttons a.btn {
    width: 120px;
  }
  a.btn.btn-default:hover {
    background: #e6e6e6;
  }
  div.padding-row {
    padding: 30px 0px;
  }
</style>
@endpush
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Danh sách sản phẩm</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>                    
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div class="row padding-row">
          <div class="col-md-12">
            <form class="form-inline" id="productFilter" action="{{route('backend::module.filter')}}" method="GET">
              <label for="category_id">Danh mục</label> 
              <select id="Product_category_id" name="category_id" class="form-control">
                {!!App\Models\Category::getListCate()!!}
              </select> 
              <label for="product_name">Tên sản phẩm</label> 
              <input type="text" name="product_name" id="product_name" class="form-control">
              <a class="btn btn-primary" id="btnProductFilter"><i class="fa fa-search"></i> Tìm</a>
            </form>
          </div>
        </div>
        <div class="row">
          <form id="settingForm" action="{{route('backend::module.setting')}}" method="POST">
            <div class="col-md-5">
              <label for="searchResult">Kết quả tìm kiếm:</label>
              <select multiple id="searchResult" class="form-control">
              </select>
            </div>
            <div class="col-md-2 text-center column-buttons">
              <a id="btnAdd" class="btn btn-default"> add <i class="fa fa-angle-right"></i> </a> <br>
              <a id="btnAddAll" class="btn btn-default"> add all <i class="fa fa-angle-double-right"></i> </a> <br>
              <a id="btnRemove" class="btn btn-default"> <i class="fa fa-angle-left"></i> remove </a> <br>
              <a id="btnRemoveAll" class="btn btn-default"><i class="fa fa-angle-double-left"></i> remove all </a>
              <a id="btnSave" class="btn btn-success"><i class="fa fa-floppy-o"></i> Save </a>
            </div>
            <div class="col-md-5">
              <label for="moduleContent">Nội dung module:</label>
              <select multiple id="moduleContent" class="form-control">
                @foreach($products as $p)
                  <option value="{{$p->id}}">{{$p->name}}</option>
                @endforeach          
              </select>
            </div>
          </form>
        </div>
        <div class="row">
          
        </div>
      </div>
    </div>
  </div>
</div>
@push('custom-script')
<script type="text/javascript">
  $(function(){
    $('#btnProductFilter').click(function(e){
      e.preventDefault();      
      runFilter('#productFilter');     
    });
    $('#product_name').keypress(function(e) {
      if (e.keyCode ==13) {
        e.preventDefault();
        runFilter('#productFilter');
        return false; 
      }
    })
    $('#btnAdd').click(function(e){
      var options = $('#searchResult option:selected');
      $("#moduleContent").append(options.clone());
    });
    $('#btnAddAll').click(function(e){
      var options = $('#searchResult option');
      $("#moduleContent").append(options.clone());
    });
    $('#btnRemove').click(function(e){
      $("#moduleContent option:selected").remove();
    });
    $('#btnRemoveAll').click(function(e){
      $("#moduleContent").html('');
    });
    $('#btnSave').click(function(e){
      saveSetting();
    })
  });
  function saveSetting() {
    var form = $('#settingForm');
    var products = [];
    $('#moduleContent option').each(function(index, ele) {
      products.push($(ele).attr('value'));
    });
    var moduleId = $('#ModuleForm input[name="id"]').val();
    $.ajax({
      url: form.attr('action'),
      data: {module_id: moduleId, selected_products: products},
      dataType: 'json',
      method: 'post',
      success: function(){
        alert('Đã lưu cài đặt module.');
      },
      error: function(){
        alert('Tạm thời không lưu được cài đặt cho module. Vui lòng liên hệ quản trị viên.');
      }
    });
  }
  function runFilter(selector){
    var formFilter = $(selector);
    $('#btnProductFilter').find('i').replaceWith('<i class="fa fa-spinner"></i>')
    $.ajax({
      url: formFilter.attr('action'),
      data: formFilter.serialize(),
      method: "get",
      dataType: 'json',
      success: function(json) {
        if (json.status == 'success') {
          if (json.data.length > 0) {
            var str = "";
            for(var i = 0; i < json.data.length; i++ ) {
              var item = json.data[i];
              str += '<option value="'+item.id+'">'+item.name+'</option>';
            }
            $('#searchResult').html(str);
          } else {
            $('#searchResult').html('');
          }
        }
      },
      error: function() {
        alert('Tạm thời ứng dụng không thể tìm kiếm dữ liệu. \nVui lòng liên hệ quản trị viên.');
      },
      complete: function(){
         $('#btnProductFilter').find('i').replaceWith('<i class="fa fa-search"></i>');
      }
    }); 
  }
</script>
@endpush
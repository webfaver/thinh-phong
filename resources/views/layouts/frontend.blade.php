<!doctype html>
<html lang="en" class="no-js">
<head>
	<meta charset="UTF-8">
	<meta name=viewport content="width=device-width,initial-scale=1">
	<link rel="stylesheet" href="/frontend/fonts/opensans/stylesheet.css" type="text/css">
	<link rel="stylesheet" href="/frontend/css/reset.css">
	
	<link rel="stylesheet" href="/packages/bootstrap/dist/css/bootstrap.css"> 
	<link rel="stylesheet" href="/packages/font-awesome/css/font-awesome.min.css"> 
	<link rel="stylesheet" href="/frontend/css/style.css">
	<link rel="stylesheet" href="/frontend/css/queries.css">
	@stack('custom-style')
	<title>@yield('title')</title>
</head>
<body>
	<div id="fb-root"></div>
	@include ('frontend.popup')
	@include ('frontend.header')
	@include ('frontend.addCart-popup')
	@yield('content')
	<script src="/packages/jquery/dist/jquery.min.js"></script>
	<script src="/packages/bootstrap/dist/js/bootstrap.js"></script>
	<script src="/frontend/js/modernizr.js"></script>	
	<script src="/packages/notifyjs/dist/notify.js"></script>	
	<script src="/frontend/js/main.js"></script>
	@stack('custom-script')
	<script type="text/javascript">
	// (function(d, s, id) {
	// 	var js, fjs = d.getElementsByTagName(s)[0];
	// 	  if (d.getElementById(id)) return;
	// 	  js = d.createElement(s); js.id = id;
	// 	  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5&appId=886210931397677";
	// 	  fjs.parentNode.insertBefore(js, fjs);
	// 	}(document, 'script', 'facebook-jssdk'));
	</script>
</body>
</html>
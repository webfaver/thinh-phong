# Chuẩn bị cho code website ruouthinhphong
1. Kiểm tra nếu máy dev chưa có composer, bower thì cài đặt
2. Dùng lệnh `git checkout master` để sử dụng code master. 
3. Chỉ dùng code trên nhánh master tải về 
4. Chạy lệnh `composer install`
5. Chạy lệnh `bower install`

# Cấu hình
1. Copy file `.env.example`  thành file `.env`
2. Mở file `.env` và sửa các cấu hình cho phù hợp với máy của bạn

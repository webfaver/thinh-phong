<?php
namespace App\Helpers;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Intervention\Image\ImageManagerStatic as Image;
use App\Models\Image as ImageModel;
use Log;

class UploadHelper
{
	/**
	 * Storage uploaded image and reszie
     * @param  Symfony\Component\HttpFoundation\File\UploadedFile $file
	 * @param  String $dir Directory to save image
	 * @return Mixed ['realPath','versions']
	 */
	public static function image(UploadedFile $file, $dir = "thumbnails")
	{
		$dirTarget ="/upload/".$dir."/"
                .date_create()->format("y/m/d/");  
        $fileName = str_random(10).".".$file->getClientOriginalExtension();
        $file->move(public_path().$dirTarget, $fileName);

        $img = Image::make(public_path().$dirTarget.$fileName);
        $width = $img->width();
        $sizes = explode(',', trim(env('IMAGE_SIZES')) );
        $versions = [];
        foreach ($sizes as $size) {
        	if ($width > $size) {
        		$img->widen($size);
        		$pieces = explode(".", $fileName);
        		$img->save(public_path().$dirTarget.$fileName.".".$size.".".$pieces[1]);
                $versions[] = $size;
        	}
        }
        return ['realPath'=>$dirTarget.$fileName, 'versions'=>$versions];
	}

    /**
     * Storage uploaded image and reszie, and save to Database
     * @param  Symfony\Component\HttpFoundation\File\UploadedFile $file
     * @param  String $dir Directory to save image
     * @return Mixed ['realPath','versions', 'model']
     */
    public static function saveImage(UploadedFile $file, $dir = "thumbnails")
    {
        $item = new ImageModel();
        $item->title = $file->getClientOriginalName();
        $upload = self::image($file, $dir);
        $item->url = $upload['realPath'];
        $item->sizes = implode(',', $upload['versions']);
        $item->save();
        $upload['model'] = $item;
        return $upload;
    }


    /**
     * Remove old images when user upload one new.
     * @param  String $filePath file path of original image.
     * @return Void           No return
     */
    public static function cleanOldImage($filePath)
    {
        $sizes = explode(',', trim(env('IMAGE_SIZES')) );
        $pieces = explode(".", $filePath);    
        foreach ($sizes as $size) {
            @unlink($filePath.".".$size.".".$pieces[count($pieces) - 1]);
        }
        @unlink($filePath);
    }

    public static function clearImage($objType, $objId)
    {
        $imgs = ImageModel::where('obj_type',$objType)
                    ->where('obj_id', $objId)
                    ->get();
        if (null === $imgs) {
            return false;
        }
        foreach ($imgs as $img) {
            self::cleanOldImage(public_path().$img->url);
            $img->delete();
        }
    }

	 /**
	 * Storage uploaded audio file
	 * @param  Symfony\Component\HttpFoundation\File\UploadedFile $file
	 * @return String File path storaged
	 */
	public static function audio(UploadedFile $file)
	{
		$dirTarget = DIRECTORY_SEPARATOR
                ."upload".DIRECTORY_SEPARATOR
                ."songs".DIRECTORY_SEPARATOR
                .date_create()->format("y".DIRECTORY_SEPARATOR."m".DIRECTORY_SEPARATOR."d".DIRECTORY_SEPARATOR);  
        $fileName = str_random(10).".".$file->getClientOriginalExtension();
        $file->move(public_path().$dirTarget, $fileName);
        return $dirTarget.$fileName;
	}

    /**
     * Storage uploaded image and reszie
     * @param  Symfony\Component\HttpFoundation\File\UploadedFile $file
     * @return String File path storaged
     */
    public static function file(UploadedFile $file, $dir = "data")
    {
        $dirTarget = DIRECTORY_SEPARATOR
                ."upload".DIRECTORY_SEPARATOR
                .$dir.DIRECTORY_SEPARATOR
                .date_create()->format("y".DIRECTORY_SEPARATOR."m".DIRECTORY_SEPARATOR."d".DIRECTORY_SEPARATOR);  
        $fileName = str_random(10).".".$file->getClientOriginalExtension();
        $file->move(public_path().$dirTarget, $fileName);

        return [
            'realPath' => $dirTarget.$fileName,
            'fileName' => $file->getClientOriginalName()
        ];
    }
}
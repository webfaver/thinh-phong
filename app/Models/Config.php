<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\MyModelTrait;
use Carbon\Carbon;
use Cache;


class Config extends Model
{	
  use MyModelTrait;
  protected $table = "config";
  protected $fillable = ['key', 'value', 'description'];

  public static function getLabels()
  {
  	return [
  		'key' => 'Khóa',
  		'value' => 'Giá trị',
  		'description' => 'Mô tả',
  	];
  }

  public static function get($key, $default = '')
  {
    if (env('CACHE_ENABLE', false)) {      
      $expiresAt = Carbon::now()->addMinutes(env('CACHE_TIME'));
      $configs = Cache::remember('config', $expiresAt, function()
      {
          return Config::all()->toArray();
      });      
      $obj = array_filter($configs, function($item) use (&$key)
      {
         return $item['key'] == $key;
      });    
      $val = array_pop($obj)['value'];
    } else {
      $conf = Config::where('key',$key)->first();
      $val = $conf ? $conf->value : null;
    }
    if ( !isset($val) ) {
      //Nếu không tìm thấy key thì tạo ra key trong db trước khi trả giá trị mặc định
      $newConfig = new Config();
      $newConfig->key = $key;
      $newConfig->value = $default;
      $newConfig->save();
      Cache::forget('config');
      return $default;
    } else {
      return $val;
    }
  }
}

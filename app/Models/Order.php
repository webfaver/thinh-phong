<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
  use  SoftDeletes;
  protected $table = "order";
  protected $fillable = ['name', 'phone', 'email', 'address'];

  const STT_NEW = 1;
  const STT_DONE = 2;

  public static function getLabels()
  {
    return [
      'name' => 'Tên', 
      'phone' => 'Số điện thoại', 
      'email' => 'Email', 
      'address' => 'Địa chỉ',
      'created_at' => 'Ngày tạo',
      'updated_at' => 'Ngày cập nhật',
    ];
  }
  public function details()
  {
    return $this->hasMany('App\Models\OrderDetail');
  }
  public function getStatusLabel()
  {
    $data = [
      self::STT_DONE  => ['success', 'Xong'],
      self::STT_NEW  => ['danger', 'Mới'],
    ];
    $html = '<span class="label label-#css#">#display#</span>';
    $html = str_replace('#css#',$data[$this->status][0], $html);
    $html = str_replace('#display#',$data[$this->status][1], $html);
    return $html;
  }
  public static function countNewOrder()
  {
    $count = Order::where('status',Order::STT_NEW)
                  ->count();
    return $count;
  }
}

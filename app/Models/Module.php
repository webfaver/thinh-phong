<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\MyModelTrait;

class Module extends Model
{
    use MyModelTrait, SoftDeletes;
    protected $table = "module";
    protected $fillable = ['name', 'type', 'desc'];

    const TYPE_SLIDER = 1;
    const TYPE_GRID = 2;
    const TYPE_SIDEBAR_BOX = 3;

    const STT_ENABLED = 1;
    const STT_DISABLED = 2;

    protected $myTypes = [
      self::TYPE_SLIDER      => "Slide sản phẩm",
      self::TYPE_GRID        => "Lưới sản phẩm",
      self::TYPE_SIDEBAR_BOX => "Box sản phẩm",
    ];
    
    public static function getLabels()
    {
      return [
        'id'      => 'ID',
        'type'    => 'Loại',
        'name'    => 'Tên',
        'desc'    => 'Mô tả',
        'setting' => 'Cài đặt',
        'status'  => 'Trạng thái',
      ];
    }
    public function getOptionsType()
    {
      $str = "";
      foreach($this->myTypes as $key => $value) {
        $selected = $key == $this->type ? 'selected' : '';
        $str .= '<option value="'.$key.'" '.$selected.'>'.$value.'</option>';
      }
      return $str;    
    }
    public function getTypeName()
    {
      return $this->myTypes[$this->type];
    }
    public static function loadModule($id)
    {
      $products = null;
      try {
        $module = Module::whereId($id)->firstOrFail();
        $setting = json_decode($module->setting, true);
        $products = Product::whereIn('id', $setting['content'])->get();
      } catch (ModelNotFoundException $e) {
        //do nothing
        $products = [];
      } finally {
        return $products;
      }
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\MyModelTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
  use SoftDeletes, MyModelTrait;
  protected $table = 'category';
	protected $fillable = ['parent_id','slug','name', 'desc', 'type'];	
	protected $dates = ['deleted_at','published_at'];

  const TYPE_PRODUCT    = 1;
  const TYPE_ARTICLE    = 2;

	public static function getLabels()
  {
  	return array(
			"slug" => "Đường dẫn thân thiện",
			"name" => "Tên danh mục",				
			"parent_id" => "Danh mục cha",				
			"desc" => "Mô tả",
			"thumbnail" => "Ảnh đại diện",
			"created_at" => "Ngày đăng",
			"updated_at" => "Ngày cập nhật",
      "type" => "Loại",
			"attr"	=> "Thuộc tính",
      "creator_id"=>"Người tạo"
  	);
  }
  public function parent()
  {
  	return $this->hasOne('App\Models\Category', 'id', 'parent_id');
  }
  public function creator()
  {
    return $this->belongsTo('App\Models\User');
  }
  public function children()
  {
  	return $this->hasMany('App\Models\Category', 'parent_id', 'id');
  }
  public function products()
  {
    return $this->hasMany('App\Models\Product');
  }
  public function getOptionsCate($select = 0)
  {
    $categories = Category::where('parent_id',null)->get();
    return $this->recurseCate($categories, 0, $select);
  }
  public static function getListCate($select = 0)
  {    
    $cate = new Category();
    return $cate->getOptionsCate();
  }
  private function recurseCate($cates, $level = 0, $select = 0){
    # we have a numerically-indexed array. go through each item:
    $html = '';
    foreach ($cates as $n) {
        if(0 == count($n->parent) || $level > 0) {
          $html .= '<option value="' . $n->id . '">' 
          . str_repeat("--", $level)
          . $n->name
          . '</option>'
          . PHP_EOL;
        }
        # if item['children'] is set, we have a nested data structure, so
        # call recurse on it.
        if (count($n->children) > 0) {
            $html .= $this->recurseCate( $n->children, $level+1);
        }        
    }

    if (0 == $level) {
      $html = '<option value="0">Chọn danh mục</option>'.PHP_EOL.$html;
      $parentVal = 'value="'. ($select == 0 ? $this->parent_id : $select).'"';
      $html = str_replace($parentVal, $parentVal.' selected', $html);
    }
    return $html;
  }

  public static function getOptionsType($valueSelected) {
    $types = [
      self::TYPE_PRODUCT => "Sản phẩm",
      self::TYPE_ARTICLE => "Bài viết",
    ];
    $str = "";
    foreach($types as $key => $value) {
      $selected = $key == $valueSelected ? 'selected' : '';
      $str .= '<option value="'.$key.'" '.$selected.'>'.$value.'</option>';
    }
    return $str;
  }
}

class human{
  protected $ten = '';
  protected $namsinh = 0;
  
  
  
  
  
  private function	age($currentYear){
	return $currentYear - $namsinh;
  }
  
  public function speak(){
	
  }
}


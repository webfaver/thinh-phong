<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Image;

trait MyModelTrait
{
  public function getTableName()
  {
    return $this->table;
  }
  public function isNew()
  {
    return null === $this->id;
  }
	public function getThumbBySize($value=128, $imgCol = 'thumbnail')
  {    
    if ($this[$imgCol] === null)
      return "/backend/images/placeholder.png";

    $sizes = explode(',', trim(env('IMAGE_SIZES')) );
    $pieces = explode(".", $this[$imgCol]);    
    foreach ($sizes as $size) {
      if ($value >= $size) {        
        return $this[$imgCol].".".$size.".".$pieces[count($pieces) - 1];        
      }
    }
    return $this[$imgCol];
  }

  public function getImgBySize($value=128, $isItSelf = false)
  {
    if (false === $isItSelf) {
      $img = Image::where('obj_type',$this->table)
                  ->where('obj_id',$this->id)
                  ->orderBy('id','ASC')
                  ->first();
    } else {
      $img = $this;
    }
    if (null === $img)
      return "/backend/images/placeholder.png";

    $sizes = explode(',',$img->sizes);
    $pieces = explode(".", $img->url);
    foreach ($sizes as $size) {
      if ($value >= $size) {        
        return $img->url.".".$size.".".$pieces[count($pieces) - 1];        
      }
    }
    return $img->url;
  }

  public function images()
  {
    $images = Image::where('obj_type',$this->table)
                    ->where('obj_id',$this->id)
                    ->orderBy('id','ASC')
                    ->get();
    return $images;
  }

  public function sumAttr($requestData) {
    $requestData = null === $requestData ? [] : $requestData;
    $sum = 0;    
    foreach ($requestData as $value) {
      $sum += $value;
    }
    $this->attr = $sum;
    return $this->attr;
  }

  /**
   * Hàm chuyển đổi giá trị attr lưu trong db thành mảng các attributes ở tầng logic
   * @return Array Mảng chứa các attributes đã bật
   * Ví dụ: 13 -> [1,4,8]
   */
  public function calAttr()
  {
    $base = $this->attr*1;
    $bina = strrev(decbin($base));
    $data =  [];
    for($i = 0; $i < strlen($bina); $i++ )
        if ( $bina[$i] == 1)
            $data[] = pow(2,$i);
    return $data;
  }

  /**
   * Return checkbox input for attributes.
   * Need an array from module $this->myAttributes 
   * @return [type] [description]
   */
  public function getAttrOptions()
  {
      $html = "";
      $i = 0;
      foreach ($this->myAttributes as $key => $value) {
          $tmp = '<div class="radio"><label><input type="checkbox" #checked# value="#value#" class="flat" name="attr[#order#]"> #display_name#</label></div>';
          $tmp = str_replace('#checked#', ($this->attr & $key) > 0 ?'checked':'', $tmp);
          $tmp = str_replace('#value#', $key, $tmp);
          $tmp = str_replace('#order#', $i++, $tmp);
          $tmp = str_replace('#display_name#', $value, $tmp);
          $html .= $tmp;
      }
      return $html;
  }
  public function strip_tags($name)
  {
    return strip_tags($this->$name, '<p><a><b><strong><i><italic><span><h1><h2><h3><h4><h5><h6><iframe><sup><sub><ul><ol><li>');
  }
}
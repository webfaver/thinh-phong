<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
  protected $table = 'order_detail';
  public function product()
  {
    return $this->hasOne('App\Models\Product','id','product_id');
  } 
  public function size()
  {
    return $this->hasOne('App\Models\ProductSize','id', 'size_id');
  } 
  public function order()
  {
    return $this->belongsTo('App\Models\Order');
  }
}

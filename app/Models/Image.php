<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\MyModelTrait;

class Image extends Model
{
  use MyModelTrait {
    getThumbBySize as getThumb;
  }
  protected $table = 'image';
  protected $fillable = ['gallery_id', 'title', 'content', 'url'];
  protected $dates = ['deleted_at'];

  public static function getLabels()
  {
    return [
      'obj_type' => '',
      'obj_id' => '',
      'title' => '',
      'desc' => '',
      'url' => '',
      'source' => '',
      'sizes' => '',
    ];
  }

  public function getThumbBySize($size=128)
  {
    return $this->getThumb($size, 'url');
  }
}

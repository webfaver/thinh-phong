<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use App\Models\MyModelTrait;
use App\Models\Role;

class User extends Authenticatable
{
    use EntrustUserTrait, MyModelTrait;
    protected $table = 'users';
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
    * The attributes excluded from the model's JSON form.
    *
    * @var array
    */
    protected $hidden = [
        'password', 'remember_token',
    ];

    const ATTR_BANNED = 1;    
    protected $myAttributes = [
        self::ATTR_BANNED => 'Khóa tài khoản',
    ];

    public static function getLabels()
    {
        return [
            'name' => 'Tên hiển thị',
            'email' => 'Địa chỉ email',
            'password' => 'Mật khẩu',
            'password_current' => 'Mật khẩu hiện tại',
            'avatar' => 'Ảnh đại diện',
            'created_at' => 'Ngày tạo',
            'updated_at' => 'Ngày cập nhật',
            'attr' => 'Trạng thái',
            'roles' => 'Quyền',
        ];
    }
    public function getRoleOptions() {
        $roles = Role::all();
        $html = "";
        foreach ($roles as $key => $role) {
            $tmp = '<div class="radio"><label><input type="checkbox" #checked# value="#value#" class="flat" name="roles[#order#]"> #display_name#</label></div>';
            $tmp = str_replace('#checked#', $this->hasRole($role->name)?'checked':'', $tmp);
            $tmp = str_replace('#value#', $role->id, $tmp);
            $tmp = str_replace('#order#', $key, $tmp);
            $tmp = str_replace('#display_name#', $role->display_name, $tmp);
            $html .= $tmp;
        }
        return $html;
    }
    
    public function isBanned()
    {
        return ($this->attr & self::ATTR_BANNED) > 0;
    }
}

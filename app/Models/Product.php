<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\MyModelTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Image;
use Auth;

class Product extends Model
{
  use SoftDeletes, MyModelTrait;
  protected $table = 'product';
  protected $fillable = ['slug', 'plu', 'category_id', 'brand', 'name', 'desc', 'price'];

  const STT_ONLINE   = 1;
  const STT_SUSPEND  = 2;

  const TYPE_WINE = 1;
  
  const ATTR_FEATURED   = 1;
  const ATTR_RECOMMEND  = 2;
  const ATTR_BEST_SALE  = 4;

  protected $myAttributes = [
    self::ATTR_FEATURED => 'Đặc sắc',
    self::ATTR_RECOMMEND => 'Nên xem',
    self::ATTR_BEST_SALE => 'Được yêu thích',
  ];

  public static function getLabels()
  {
    return [
      'slug' => 'Đường dẫn thân thiện',
      'plu' => 'Mã sản phẩm',
      'category_id' => 'Nhóm',
      'brand' => 'Thương hiệu',
      'name' => 'Tên',
      'desc' => 'Mô tả',
      'thumbnail' => 'Ảnh đại diện',
      'status' => 'Trạng thái',
      'attr' => 'Thuộc tính',
      'type' => 'Loại',
      'price' => 'Giá',
      'sizes' => 'Các kích cỡ',
      'created_at' => 'Ngày tạo',
      'updated_at' => 'Ngày cập nhật',
      'creator_id' => 'Người tạo',
    ];
  }
  public function category()
  {
    return $this->belongsTo('App\Models\Category');
  }
  public function creator()
  {
    return $this->belongsTo('App\Models\User');
  }
  public function sizes()
  {
    return $this->hasMany('App\Models\ProductSize');
  }   
  public function getCategoryOptions() {
    if ($this->isNew()) {
      $cate = new Category();
      return $cate->getOptionsCate();
    } else {
      return $this->category->getOptionsCate($this->category->id);
    }
  }

  public function getStatusLabel()
  {
    $data = [
      self::STT_SUSPEND  => ['danger', 'Tạm ngưng bán'],
      self::STT_ONLINE  => ['success', 'Đang bán'],
    ];
    $html = '<span class="label label-#css#">#display#</span>';
    $html = str_replace('#css#',$data[$this->status][0], $html);
    $html = str_replace('#display#',$data[$this->status][1], $html);
    return $html;
  }

  public function fDesc()
  {
    return strip_tags($this->desc, '<p><a><b><strong><i><italic><span><h1><h2><h3><h4><h5><h6><iframe><sup><sub><ul><ol><li>');
  }
  public function fPrice()
  {
    return $this->price == 0 ? "Liên hệ" : number_format($this->price,0,",",".").'&#x20ab;';
  }
  public function fStock()
  {
    if ($this->status == self::STT_ONLINE) {
      return '<span class="stock">Sản phẩm đang có tại cửa hàng</span>';
    } 
    return '<span class="out-stock">Sản phẩm hiện không có tại cửa hàng</span>';
  }
}
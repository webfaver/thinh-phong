<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductSize extends Model
{
  protected $table = 'product_size';
  protected $fillable = ['name', 'price'];
  public $timestamps = false;

  public function product()
  {
    return $this->belongsTo('App\Models\product');
  }
}

<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Entrust;
class ModuleFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Entrust::can('edit-config');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type'  => 'required|integer|min:1',
            'name' => 'required|max:128',
            'description' => 'max:500',
        ];
    }
}

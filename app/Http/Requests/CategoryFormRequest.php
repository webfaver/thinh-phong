<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Entrust;

class CategoryFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|min:3',
            'slug'=>"required|min:3|alpha_dash|unique:category,slug,".$this->get('id'),      
            'thumbnail' => 'image:jpeg,png,bmp|max:10000',              
            'type' => 'required|min:1|max:3',       
        ];
    }
}

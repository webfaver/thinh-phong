<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Auth;
use Entrust;
use Log;
use App\Models\Product;

class ProductFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {        
        return [          
            'slug' => "required|min:3|alpha_dash|unique:product,slug,".$this->get('id'), 
            'category_id' => 'required|integer|min:1', 
            'brand' => 'max:255', 
            'name' => 'required|max:255', 
            'desc' => 'required', 
            'price' => 'integer',
            'type' => 'integer', 
            'quantity' => 'integer',
            'sizes'=>   'required|product_sizes',
        ];
    }    
}

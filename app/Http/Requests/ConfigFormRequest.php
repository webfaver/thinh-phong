<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Entrust;
class ConfigFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Entrust::can('edit-config');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'key'=> 'required|max:64|unique:config,key,'.$this->get('id'),
            'value' => 'required|max:500',
            'description' => 'max:500'
        ];
    }
}

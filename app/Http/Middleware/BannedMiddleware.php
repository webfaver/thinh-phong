<?php

namespace App\Http\Middleware;
use Auth;
use Closure;

class BannedMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user()->isBanned()) {
            Auth::logout();
            return abort(401);
        }
        return $next($request);
    }
}

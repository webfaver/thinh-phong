<?php

namespace App\Http\Middleware;

use Closure;
use Cache;

class HtmlCache
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //@TODO clear cache when publish something
        $cacheKey = "HTML_CACHE:".$request->path();
        if (Cache::tags('views')->has($cacheKey)) {
            return response(Cache::tags('views')->get($cacheKey,'Somethings wrong!'), 200);
        }
        return $next($request);
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group([], function(){
  Route::auth();
  Route::get('/', ['as'=>'index', 'uses'=> 'HomeController@index']);
  Route::get('/product/{slug}.html', ['as'=>'productDetail', 'uses' => 'HomeController@productDetail']);
  Route::get('/collection/{slug}', ['as'=>'collection', 'uses' => 'HomeController@collection']);
  Route::get('/search', ['as'=>'search', 'uses' => 'HomeController@search']);
  Route::get('/cart', ['as'=>'cart',  'uses' => 'HomeController@cart']);
  Route::post('/order', ['as'=>'order',  'uses' => 'HomeController@order']);
});


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['prefix'=>'/dashboard', 'as'=>'backend::', 'middleware' => ['auth','banned']], function () {
  Route::get('/', ['as'=>'dashboard', 'uses'=>'HomeController@dashboard']);
  Route::controller('category', 'Backend\CategoryController',[
      'getIndex'  => 'category.home',
      'getAdd'    => 'category.add',
      'postStore' => 'category.save',
      'getShow'   => 'category.show',
      'getEdit'   => 'category.edit',
      'postUpdate'=> 'category.update',
      'deleteDestroy'=> 'category.remove',
      'getTrash'  => 'category.trash',
      'getRestore'  => 'category.restore',
  ]);
  Route::controller('product', 'Backend\ProductController',[
      'getIndex'      => 'product.home',
      'getJob'        => 'product.job',
      'getAdd'        => 'product.add',
      'postStore'     => 'product.save',
      'getShow'       => 'product.show',
      'getEdit'       => 'product.edit',
      'postUpdate'    => 'product.update',
      'deleteDestroy' => 'product.remove',
      'getTrash'      => 'product.trash',
      'getRestore'    => 'product.restore',
      'getMakeOnline' => 'product.online',
      'getMakeSuspend'=> 'product.suspend',
  ]);
  Route::controller('image', 'Backend\ImageController',[
      'getIndex'    => 'image.home',
      'postUpload'  => 'image.upload',
      'deleteItem'  => 'image.deleteItem',
      'postUpdateItem' => 'image.updateItem',
  ]);

  Route::controller('config' ,'Backend\ConfigController', [
      'getIndex'      => 'config.home',
      'getAdd'        => 'config.add',
      'getEdit'       => 'config.edit',
      'postStorage'   => 'config.save',
      'postUpdate'    => 'config.update',
      'deleteDestroy' => 'config.remove',
  ]);
  Route::controller('order' ,'Backend\OrderController', [
      'getIndex'      => 'order.home',
      'getShow'       => 'order.show',      
      'getDone'       => 'order.done',      
      'getNone'       => 'order.none',      
      'deleteDestroy' => 'order.remove',
      'getTrash'      => 'order.trash',
      'getRestore'    => 'order.restore',
  ]);
  Route::controller('module' ,'Backend\ModuleController', [
      'getIndex'      => 'module.home',
      'getAdd'        => 'module.add',
      'getEdit'       => 'module.edit',
      'postStorage'   => 'module.save',
      'postUpdate'    => 'module.update',
      'deleteDestroy' => 'module.remove',
      'getTrash'      => 'module.trash',
      'getRestore'    => 'module.restore',
      'getFilter'     => 'module.filter',
      'postSetting'   => 'module.setting',
  ]);
  Route::controller('user', 'Backend\UserController',[
      'getIndex'    => 'user.home',
      'getAdd'      => 'user.add',
      'postStore'   => 'user.save',
      'getShow'     => 'user.show',
      'getEdit'     => 'user.edit',
      'postUpdate'  => 'user.update',
      'deleteBan'   => 'user.lock',
      'getJail'     => 'user.jail',
      'getUnlock'   => 'user.unlock',
      'getProfile'  => 'user.profile',
      'postProfile' => 'user.updateProfile',
  ]);
  Route::controller('role', 'Backend\RoleController',[
      'getIndex'    => 'role.home',
      'getEdit'     => 'role.edit',
      'postUpdate'  => 'role.update',
  ]);
});
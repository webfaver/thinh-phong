<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryFormRequest;
use App\Models\Category;
use App\Helpers\UploadHelper;
use App\Models\Image;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getIndex()
    {
        $categories = Category::all();
        return view('backend.category.index', ['categories'=>$categories, 'listName'=>'Danh sách các danh mục']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getAdd()
    {
        $category = new Category();
        return view('backend.category.add', ['category' => $category]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CategoryFormRequest  $request
     * @return Response
     */
    public function postStore(CategoryFormRequest $request)
    {
        $category = new Category($request->all());
        $category->creator_id = Auth::user()->id;
        if ($request->input('parent_id') == 0) {
            $category->parent_id = null;
        }
        $category->save();
        
        if ( null !== $request->file('thumbnail') ) {             
            $upload = UploadHelper::saveImage($request->file('thumbnail'));
            $img = $upload['model'];
            $img->obj_type = $category->getTableName();
            $img->obj_id = $category->id;
            $img->save();
        }

        return redirect( route('backend::category.edit',[$category->id]) )
                ->with('status', 'Thêm danh mục thành công!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function getShow($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function getEdit($id)
    {
        $category = Category::whereId($id)->firstOrFail();
        return view('backend.category.edit', array('category'=>$category));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  SongFormRequest  $request
     * @return Response
     */
    public function postUpdate(CategoryFormRequest $request)
    {        
        $category = Category::whereId($request->get('id'))->firstOrFail();
        $category->fill($request->input());
        $category->parent_id = $category->parent_id == 0 ? null : $category->parent_id;
        $category->save();
        
        if (null !== $request->get('thumbnail_remove')) {
            UploadHelper::clearImage($category->getTable(), $category->id);
        }
        if ( null !== $request->file('thumbnail') ) { 
            $upload = UploadHelper::saveImage($request->file('thumbnail'));
            $img = $upload['model'];
            $img->obj_type = $category->getTableName();
            $img->obj_id = $category->id;
            $img->save();
        }
        
        return redirect(route('backend::category.edit',[$category->id]) )->with('status', 'Cập nhật thành công!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function deleteDestroy($id)
    {
        $cate = Category::whereId($id)->firstOrFail();        
        if ($cate->delete()) {
            return array("hasError"=>false, 'message' => '<i class="fa fa-check"></i> Đã vào thùng rác');
        }
        return array("hasError"=>true, 'message'=> '<i class="fa fa-times-circle"></i> Lỗi: không xóa được.');
    }
    /**
     * Display resources have been moved to trash
     *
     * @return Response
     */
    public function getTrash()
    {   
        $categories = Category::onlyTrashed()->get();
        return view('backend.category.index', ['categories' => $categories, 'listName'=>'Thùng rác']);
        
    }

    /**
     * Restore the specified resource from trash.
     *
     * @param  int  $id
     * @return Response
     */
    public function getRestore($id)
    {
        $cate = Category::whereId($id)->withTrashed()->firstOrFail();
        $cate->restore();
        return redirect( route('backend::category.trash') )->with('status', 'Khôi phục thành công!');
    }
}

<?php 
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Config;
use App\Http\Requests\ConfigFormRequest;
/**
* Make config tool
*/
class ConfigController extends Controller
{
	public function getIndex()
	{
		$configs = Config::orderBy('key','ASC')->get();
		return view('backend.config.index', ['configs'=>$configs]);
	}

	public function getAdd()
	{
    return redirect(route('backend::config.home'));
	}
	
	/**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function getEdit($id)
  {
      $config = Config::whereId($id)->firstOrFail();
      return view('backend.config.edit', array('config'=>$config));
  }

	/**
   * Store a newly created resource in storage.
   *
   * @param  App\Http\Requests\ConfigFormRequest  $request
   * @return \Illuminate\Http\Response
   */
  public function postStorage(ConfigFormRequest $request)
	{
		$config = new Config($request->all());
		$config->save();
    return redirect( route('backend::config.edit',$config->id) )->with('status', 'Thêm cấu hình thành công!');
	}

	/**
   * Update the specified resource in storage.
   *
   * @param  App\Http\Requests\ConfigFormRequest  $request
   * @return \Illuminate\Http\Response
   */
	public function postUpdate(ConfigFormRequest $request)
	{
		$config = Config::whereId($request->get('id'))->firstOrFail();
    $config->fill($request->input());
		$config->save();
    return redirect( route('backend::config.edit',$config->id) )->with('status', 'Cập nhật cấu hình thành công!');
	}

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function deleteDestroy($id)
  {
      $config = Config::whereId($id)->firstOrFail(); 
      if ($config->delete()) {
          return array("hasError"=>false, 'message' => '<i class="fa fa-check"></i> Đã xóa');
      }
      return array("hasError"=>true, 'message'=> '<i class="fa fa-times-circle"></i> Lỗi: không xóa được.');
  }
}
<?php 

namespace App\Http\Controllers\Backend;

use Auth;
use Validator;
use Hash;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\UserFormRequest;
use App\Models\User;
use App\Helpers\UploadHelper;

class UserController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getIndex()
    {
        $users = User::whereRaw('(attr & '.User::ATTR_BANNED.') = 0')->paginate(env('ITEM_PER_PAGE',20));
        return view('backend.user.index', ['users'=>$users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getAdd()
    {
        $user = new User();
        return view('backend.user.add', ['user' => $user]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  UserFormRequest  $request
     * @return Response
     */
    public function postStore(UserFormRequest $request)
    {
        $user = User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password')),
        ]);
        //Lưu các thuộc tính trạng thái
        $user->sumAttr($request->get('attr'));
        $user->save();

        if ( null !== $request->file('avatar') ) {
            $upload = UploadHelper::saveImage($request->file('avatar'), 'avatars');
            $img = $upload['model'];
            $img->obj_type = $user->getTableName();
            $img->obj_id = $user->id;
            $img->save();
        }  

        foreach ($request->get('roles') as $key => $roleid) {
            $user->roles()->attach($roleid);
        }
        return redirect( route('backend::user.edit',[$user->id]) )->with('status', 'Thêm user thành công!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function getShow($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function getEdit($id)
    {
        $user = User::whereId($id)->firstOrFail();
        return view('backend.user.edit', array('user'=>$user));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UserFormRequest  $request
     * @return Response
     */
    public function postUpdate(UserFormRequest $request)
    {        
        $user = User::whereId($request->get('id'))->firstOrFail();
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        if ('' != $request->get('password')) {
          $user->password = Hash::make($request->get('password'));
        }
        
        //Xử lý avatar
        if (null !== $request->get('avatar_remove')) {
            UploadHelper::clearImage($user->getTable(), $user->id);
        }
        if ( null !== $request->file('avatar') ) { 
            UploadHelper::clearImage($user->getTable(), $user->id);
            $upload = UploadHelper::saveImage($request->file('avatar'), 'avatars');
            $img = $upload['model'];
            $img->obj_type = $user->getTableName();
            $img->obj_id = $user->id;
            $img->save();
        }
        
        //Lưu các thuộc tính trạng thái
        $user->sumAttr($request->get('attr'));

        // Lưu phân quyền
        $user->roles()->sync($request->get('roles'));

        $user->save();
        return redirect(route('backend::user.edit',[$user->id]) )->with('status', 'Cập nhật thành công!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function deleteBan($id)
    {
        $user = User::whereId($id)->firstOrFail();   
        $attrData = $user->calAttr();
        if (in_array(User::ATTR_BANNED,$attrData)) {
            return array("hasError"=>true, 'message'=> '<i class="fa fa-times-circle"></i> Lỗi: User này đang bị khóa rồi. Không cần khóa lại!.');
        } 
        $attrData[] = User::ATTR_BANNED;
        $user->sumAttr($attrData);
        if ($user->save()) {
            return array("hasError"=>false, 'message' => '<i class="fa fa-check"></i> Đã Khóa!');
        }
        return array("hasError"=>true, 'message'=> '<i class="fa fa-times-circle"></i> Lỗi: không khóa được.');
    }
    /**
     * Display list user was banned
     *
     * @return Response
     */
    public function getJail()
    {   
        $users =  $users = User::whereRaw('(attr & '.User::ATTR_BANNED.') > 0')->paginate(env('ITEM_PER_PAGE',20));
        return view('backend.user.index', ['users' => $users]);
        
    }

    /**
     * Unlock user from the jail
     *
     * @param  int  $id
     * @return Response
     */
    public function getUnlock($id)
    {
        $user = User::whereId($id)->firstOrFail();
        $attrData = $user->calAttr();
        foreach ($attrData as $key => $value) {
            if ($value == User::ATTR_BANNED)
                $attrData[$key] = 0;
        }
        $user->sumAttr($attrData);
        if ($user->save())
            return redirect( route('backend::user.jail') )->with('status', ' Mở khóa thành công!');
        return redirect( route('backend::user.jail') )->with('error', ' Lỗi không thể mở khóa cho tài khoản '.$user->email);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function getProfile()
    {
        $user = User::whereId(Auth::user()->id)->firstOrFail();
        return view('backend.user.profile', array('user'=>$user));
    }

     /**
     * Update the specified resource in storage.
     *
     * @param  UserFormRequest  $request
     * @return Response
     */
    public function postProfile(UserFormRequest $request)
    {        
        $user = User::whereId(Auth::user()->id)->firstOrFail();
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        if ('' != $request->get('password_current')) {
            if (!Hash::check($request->get('password_current'), $user->password)) {
                return redirect(route('backend::user.profile'))                    
                    ->with('error', 'Sai mật khẩu hiện tại');
            }
            $user->password = Hash::make($request->get('password'));
        }
        
        //Xử lý avatar
        if (null !== $request->get('avatar_remove')) {
            UploadHelper::cleanOldImage(public_path().$user->avatar);
            $user->avatar = null;
        }
        if ( null !== $request->file('avatar') ) { 
            if (null !== $user->avatar){
                UploadHelper::cleanOldImage(public_path().$user->avatar);                
            }
            $user->avatar = UploadHelper::image($request->file('avatar'), 'avatars');
        }

        $user->save();
        return redirect(route('backend::user.profile') )->with('status', 'Cập nhật thành công!');
    }
}
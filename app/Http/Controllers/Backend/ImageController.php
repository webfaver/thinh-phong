<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Helpers\UploadHelper;
use App\Models\Image;
use Auth;
use Youtube;

class ImageController extends Controller
{
    /**
     * A page list all Gallery.
     *
     * @return Response
     */
    public function getIndex()
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function postUpload(Request $request)
    {
        if ($file = $request->file('file')){
            $item = new Image();
            $item->obj_type = $request->get('obj_type');
            $item->obj_id = $request->get('obj_id');
            $item->title = $file->getClientOriginalName();
            $item->desc = '';
            $upload = UploadHelper::image($file, 'images');
            $item->url = $upload['realPath'];
            $item->sizes = implode(',', $upload['versions']);
            $item->save();
            return ['status'=>'success','message'=>'','data'=>$item->toArray()];
        }
        return ['status'=>'error','message'=>'Can not save file','data'=>[]];
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function postUpdateItem(Request $request)
    {
        try {
            $item = Image::whereId($request->get('item_id'))->firstOrFail();
            $item->title = $request->get('item_title');
            $item->desc = $request->get('item_content');
            $item->source = $request->get('item_link');
            $item->save();
            return ['status'=>'success','message'=>'Cập nhật thành công','data'=>$item->toArray()];
        } catch (ModelNotFoundException $e) {
            return ['status'=>'error','message'=>'Không tìm thấy dữ liệu cần cập nhật','data'=>[]];
        }
    }

    /**
     * Remove the specified item from gallery.
     *
     * @param  int  $id
     * @return Response
     */
    public function deleteItem($id)
    {
        try {
            $galleryItem = Image::whereId($id)->firstOrFail();
            if ($galleryItem->delete()) {
                UploadHelper::cleanOldImage(public_path().$galleryItem->url);
                return array("status"=>'success', 'message' => 'Đã xóa.');
            } else {
                return array("status"=>'error', 'message' => 'Lỗi không xác định: không thể xóa item này.');
            }
        } catch (ModelNotFoundException $e) {
            return array("status"=>'error', 'message'=> 'Không tìm thấy item cần xóa. Có thể item vừa được xóa trước đó.');
        }
    }
}

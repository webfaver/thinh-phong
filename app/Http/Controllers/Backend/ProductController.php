<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProductFormRequest;
use App\Helpers\UploadHelper;
use App\Models\Product;
use App\Models\ProductSize;
use App\Models\Category;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getIndex()
    {       
        $products = Product::orderBy('updated_at','DESC')
            ->paginate(env('ITEM_PER_PAGE',20));
        return view('backend.product.index', compact('products') );
    }
    
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function getAdd()
    {
        $product = new Product();
        return view('backend.product.add', compact('product'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ProductFormRequest  $request
     * @return Response
     */
    public function postStore(ProductFormRequest $request)
    {
        $product = new Product($request->all());
        $product->status = Product::STT_ONLINE;
        $product->creator_id = Auth::user()->id;    
        $product->sumAttr($request->get('attr'));    
        $product->save();
        foreach($request->sizes as $key=>$size) {
            if ($size != '') {
                $productSize = new ProductSize();
                $productSize->name = $size;
                $productSize->price = $request->add_prices[$key];
                $productSize->product()->associate($product);
                $productSize->save();
            }
        }
        
        return redirect( route('backend::product.edit',[$product->id]) )->with('status', 'Thêm sản phẩm thành công!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function getShow($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function getEdit($id)
    {
        $product = Product::whereId($id)->firstOrFail();
        return view('backend.product.edit', array('product'=>$product));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  ProductFormRequest  $request
     * @return Response
     */
    public function postUpdate(ProductFormRequest $request)
    {
        $product = Product::whereId($request->get('id'))->firstOrFail();
        $product->fill($request->input());
        $product->sumAttr($request->get('attr'));  
        $product->save();        
        foreach($request->get('sizes_id') as $key=>$id) {
            if ($id == "") {
                $productSize = new ProductSize();
            }
            else {
                $productSize = ProductSize::whereId($id)->first();
            }
            $productSize->name = $request->sizes[$key];
            $productSize->price = $request->add_prices[$key];
            $productSize->product_id = $product->id;
            $productSize->save();
        }
        return redirect( route('backend::product.edit',[$product->id]) )->with('status', 'Cập nhật thành công!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function deleteDestroy($id)
    {
        $product = Product::whereId($id)->firstOrFail(); 
        if ($product->delete()) {
            return array("hasError"=>false, 'message' => '<i class="fa fa-check"></i> Đã vào thùng rác');
        }
        return array("hasError"=>true, 'message'=> '<i class="fa fa-times-circle"></i> Lỗi: không xóa được.');
    }
    /**
     * Display resources have been moved to trash
     *
     * @return Response
     */
    public function getTrash()
    {   
        $products = Product::onlyTrashed()
            ->where('creator_id',Auth::user()->id)
            ->paginate(env('ITEM_PER_PAGE',20));
        return view('backend.product.index', compact('products') );
    }

    /**
     * Restore the specified resource from trash.
     *
     * @param  int  $id
     * @return Response
     */
    public function getRestore($id)
    {
        $product = Product::whereId($id)->withTrashed()->firstOrFail();
        $product->restore();
        return redirect( route('backend::product.trash') )->with('status', 'Khôi phục thành công!');
    }

    /**
     * Send to Tongbientap to publish.
     *
     * @param  int  $id
     * @return Response
     */
    public function getMakeOnline($id)
    {
        $product = Product::whereId($id)->firstOrFail();       
        $product->status = Product::STT_ONLINE;
        $product->save();
        return redirect(route('backend::product.edit',[$product->id]))->with('status', 'Sản phẩm đã online. Hiển thị như còn hàng trong kho.');
    }

    /**
     * Cancel the submit product to continute edit
     *
     * @param  int  $id
     * @return Response
     */
    public function getMakeSuspend($id)
    {
        $product = Product::whereId($id)->firstOrFail();       
        $product->status = Product::STT_SUSPEND;
        $product->save();
        return redirect( route('backend::product.edit',[$product->id]) )->with('status', 'Đã báo tạm ngưng kinh doanh sản phẩm. Hiển thị như hết hàng.');
    }    
}

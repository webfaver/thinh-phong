<?php 
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Module;
use App\Models\Product;
use App\Http\Requests\ModuleFormRequest;

/**
* Make module tool
*/
class ModuleController extends Controller
{
	public function getIndex()
	{
		$modules = Module::orderBy('type','ASC')->get();
		return view('backend.module.index', ['modules'=>$modules]);
	}

	public function getAdd()
	{
    $module = new Module();
    return view('backend.module.add', compact('module') );
	}
	
	/**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function getEdit($id)
  {
      $module = Module::whereId($id)->firstOrFail();
      $setting = json_decode($module->setting, true);
      if(count($setting) > 0 && isset($setting['content'])) {
        $products = Product::whereIn('id',$setting['content'])->get();
      } else {
        $products = [];
      }
      return view('backend.module.edit', compact('module','products'));
  }

	/**
   * Store a newly created resource in storage.
   *
   * @param  App\Http\Requests\ModuleFormRequest  $request
   * @return \Illuminate\Http\Response
   */
  public function postStorage(ModuleFormRequest $request)
	{
		$module = new Module($request->all());
    $module->status = Module::STT_ENABLED;
		$module->save();
    return redirect( route('backend::module.edit',$module->id) )->with('status', 'Thêm module thành công!');
	}

	/**
   * Update the specified resource in storage.
   *
   * @param  App\Http\Requests\ModuleFormRequest  $request
   * @return \Illuminate\Http\Response
   */
	public function postUpdate(ModuleFormRequest $request)
	{
		$module = Module::whereId($request->get('id'))->firstOrFail();
    $module->fill($request->input());
		$module->save();
    return redirect( route('backend::module.edit',$module->id) )->with('status', 'Cập nhật module thành công!');
	}

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function deleteDestroy($id)
  {
      $module = Module::whereId($id)->firstOrFail(); 
      if ($module->delete()) {
          return array("hasError"=>false, 'message' => '<i class="fa fa-check"></i> Đã xóa');
      }
      return array("hasError"=>true, 'message'=> '<i class="fa fa-times-circle"></i> Lỗi: không xóa được.');
  }
  /**
   * Display resources have been moved to trash
   *
   * @return Response
   */
  public function getTrash()
  {   
      $categories = Module::onlyTrashed()->get();
      return view('backend.category.index', ['categories' => $categories, 'listName'=>'Thùng rác']);
      
  }

  /**
   * Restore the specified resource from trash.
   *
   * @param  int  $id
   * @return Response
   */
  public function getRestore($id)
  {
      $cate = Module::whereId($id)->withTrashed()->firstOrFail();
      $cate->restore();
      return redirect( route('backend::category.trash') )->with('status', 'Khôi phục thành công!');
  }
  /**
   * Return product list by fitler
   * @param /Illuminate/Http/Request
   * @return Response
   */
  public function getFilter(Request $request)
  {
    $cateId = $request->get('category_id');
    $productName = strtolower($request->get('product_name'));
    

    if ( $cateId != 0 && isset($productName)) {
      $products = Product::join('category','product.category_id','=','category.id')
                  ->whereRaw('LOWER(product.name) like ?',['%'.$productName.'%'])
                  ->whereRaw('category.id = ?',[$cateId])
                  ->select('product.*')
                  ->get()
                  ->toArray();
    } else {
      if (!isset($productName)) {
        $products = Product::where('category_id', $cateId)
          ->get
          ->toArray();
      } elseif( $cateId == 0 )  {
        $products = Product::whereRaw('name like ?', ['%'.$productName.'%'])
          ->get()
          ->toArray();
      } else {
        $products = [];
      }
    }

    return ['status'=>'success', 'data'=>$products];
  }
  public function postSetting(Request $request)
  {
    $module = Module::whereId($request->get('module_id'))->firstOrFail();
    $module->setting = json_encode([
      'content'=> $request->get('selected_products')
    ]);
    $module->save();
    return ['status'=>'success','message'=>''];
  }
}
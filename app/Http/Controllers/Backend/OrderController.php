<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\OrderFormRequest;
use App\Models\Order;
use App\Helpers\UploadHelper;
use App\Models\Image;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('csrf4Get', ['only'=>[
            'getDone',
            'getNone'
        ]]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getIndex()
    {
        $orders = Order::orderBy('status','DESC')
            ->orderBy('updated_at','DESC')
            ->paginate(env('ITEM_PER_PAGE',20));
        return view('backend.order.index', compact('orders'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function getShow($id)
    {
        $order = Order::whereId($id)->firstOrFail();
        return view('backend.order.show',compact('order'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function getEdit($id)
    {
        $order = Order::whereId($id)->firstOrFail();
        return view('backend.order.edit', compact('order'));
    }

    public function getDone($id)
    {
        $order = Order::whereId($id)->firstOrFail();
        $order->status = Order::STT_DONE;
        $order->save();
        return redirect(route('backend::order.show',[$order->id]))->with('status','Đã đánh dấu đơn hàng xử lý xong');
    }
    public function getNone($id)
    {
        $order = Order::whereId($id)->firstOrFail();
        $order->status = Order::STT_NEW;
        $order->save();
        return redirect(route('backend::order.show',[$order->id]))->with('status','Đã chuyển đơn hàng thành trạng thái chưa xử lý');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  SongFormRequest  $request
     * @return Response
     */
    public function postUpdate(OrderFormRequest $request)
    {        
        $order = Order::whereId($request->get('id'))->firstOrFail();
        $order->fill($request->input());
        $order->save();
        return redirect(route('backend::order.edit',[$order->id]) )->with('status', 'Cập nhật thành công!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function deleteDestroy($id)
    {
        $order = Order::whereId($id)->firstOrFail();        
        if ($order->delete()) {
            return array("hasError"=>false, 'message' => '<i class="fa fa-check"></i> Đã vào thùng rác');
        }
        return array("hasError"=>true, 'message'=> '<i class="fa fa-times-circle"></i> Lỗi: không xóa được.');
    }
    /**
     * Display resources have been moved to trash
     *
     * @return Response
     */
    public function getTrash()
    {   
        $orders = Order::onlyTrashed()->get();
        return view('backend.order.index', ['orders' => $orders, 'listName'=>'Thùng rác']);
        
    }

    /**
     * Restore the specified resource from trash.
     *
     * @param  int  $id
     * @return Response
     */
    public function getRestore($id)
    {
        $order = Order::whereId($id)->withTrashed()->firstOrFail();
        $order->restore();
        return redirect( route('backend::order.trash') )->with('status', 'Khôi phục thành công!');
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Requests\OrderFormRequest;
use App\Helpers\UploadHelper;
use App\Models\Category;
use App\Models\Product;
use App\Models\Order;
use App\Models\OrderDetail;
use Carbon\Carbon;
use CConfig;

class HomeController extends Controller
{
  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Http\Response
   */
  public function dashboard()
  {
    return view('backend.dashboard');
  }

  /**
   * The index page of frontend
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $categories = Category::select(['id','parent_id','name', 'slug'])
      ->get()
      ->toArray();
    $menu = [];
    foreach ($categories as $cate) {
      $cate['child'] = [];
      if ( null == $cate['parent_id'] )
        $menu[] = $cate;
      else 
        foreach($menu as $index=>$item) {
          if ($item['id'] == $cate['parent_id'])
            $menu[$index]['child'][] = $cate;
        }
    }
    $featured = Product::whereRaw('(attr & ?) > 0', [Product::ATTR_FEATURED])
      ->where('status', Product::STT_ONLINE)
      ->get();
    return view('frontend.index', compact('menu','featured'));
  }

  /**
   * productDetail Show page for products detail infomation
   * @param  String $slug products slug
   * @return \Illuminate\Http\Response
   */
  public function productDetail($slug)
  {
    $categories = Category::select(['id','parent_id','name', 'slug'])
      ->get()
      ->toArray();
    $menu = [];
    foreach ($categories as $cate) {
      $cate['child'] = [];
      if ( null == $cate['parent_id'] )
        $menu[] = $cate;
      else 
        foreach($menu as $index=>$item) {
          if ($item['id'] == $cate['parent_id'])
            $menu[$index]['child'][] = $cate;
        }
    }
    $product = Product::where('slug', $slug)      
      ->firstOrFail();

    $relatedProducts = Product::where('category_id',$product->category_id)
      ->orderBy('updated_at', 'DESC')
      ->take(4)
      ->get();
    return view('frontend.product-detail', compact('product','menu','relatedProducts'));
  }

   /**
   * productDetail Show category page
   * @param  String $slug categorys slug
   * @return \Illuminate\Http\Response
   */
  public function collection($slug)
  {
    $category = Category::where('slug',$slug)->firstOrFail();
    $categories = Category::select(['id','parent_id','name', 'slug'])
      ->get()
      ->toArray();
    $menu = $this->getMenu();
    $products = $category->products;
    
    $bestSale = Product::where('category_id',$category->id)
                ->whereRaw('(attr & ?) > 0',[Product::ATTR_BEST_SALE])
                ->orderBy('updated_at', 'DESC')
                ->first();
    

    $featured = Product::where('category_id',$category->id)
                ->whereRaw('(attr & ?) > 0',[Product::ATTR_FEATURED])
                ->orderBy('updated_at', 'DESC')
                ->first();

    $recommend = Product::where('category_id',$category->id)
                ->whereRaw('(attr & ?) > 0',[Product::ATTR_RECOMMEND])
                ->orderBy('updated_at', 'DESC')
                ->first();

    return view('frontend.collection', compact('category', 'products','menu','slug', 'featured', 'recommend', 'bestSale') );
  }  

  /**
   * Show cart
   * @return \Illuminate\Http\Response
   */
  public function cart()
  {
    $categories = Category::select(['id','parent_id','name', 'slug'])
      ->get()
      ->toArray();
    $menu = [];
    foreach ($categories as $cate) {
      $cate['child'] = [];
      if ( null == $cate['parent_id'] )
        $menu[] = $cate;
      else 
        foreach($menu as $index=>$item) {
          if ($item['id'] == $cate['parent_id'])
            $menu[$index]['child'][] = $cate;
        }
    }
    
    return view('frontend.cart', compact('menu'));
  }

  /**
   * Place order
   * @param OrderFormRequest $request
   * @return redirect
   */
  public function order(OrderFormRequest $request)
  {
    $order = new Order($request->all());
    $order->status = Order::STT_NEW;
    $order->save();
    if (count($request->product_id) > 1) {
      for ($i = 1; $i < count($request->product_id); $i++) {
        $orderDetail = new OrderDetail();
        $orderDetail->order_id = $order->id;
        $orderDetail->product_id = $request->product_id[$i];
        $orderDetail->size_id = $request->size_id[$i];
        $orderDetail->quantity = $request->quantity[$i];
        $product = Product::whereId($request->product_id[$i])->first();        
        $orderDetail->price = null == $product ? 0 : $product->price;
        $orderDetail->save();
      }
    }
    return redirect(route('cart'))->with('status','Yêu cầu đã được ghi nhận. Nhân viên của chúng tôi sẽ liên lạc với bạn để cung cấp thông tin hướng dẫn. Cảm ơn!');
  }
  public function search()
  {
    $name = \Request::get('name');
    $menu = $this->getMenu();
    $products = Product::where('name', 'LIKE','%'.$name.'%')->orderBy('name')->get();

    $bestSale = Product::whereRaw('(attr & ?) > 0',[Product::ATTR_BEST_SALE])
               ->orderBy('updated_at', 'DESC')
               ->first();
    

    $featured = Product::whereRaw('(attr & ?) > 0',[Product::ATTR_FEATURED])
                ->orderBy('updated_at', 'DESC')
                ->first();

    $recommend = Product::whereRaw('(attr & ?) > 0',[Product::ATTR_RECOMMEND])
                ->orderBy('updated_at', 'DESC')
                ->first();

    return view('frontend.search', compact('products','name','bestSale','featured','recommend','menu'));
  }
  
  public function getMenu(){
     $categories = Category::select(['id','parent_id','name', 'slug'])
      ->get()
      ->toArray();
    $menu = [];
    foreach ($categories as $cate) {
      $cate['child'] = [];
      if ( null == $cate['parent_id'] )
        $menu[] = $cate;
      else 
        foreach($menu as $index=>$item) {
          if ($item['id'] == $cate['parent_id'])
            $menu[$index]['child'][] = $cate;
        }
    }
    return $menu;
  }
}
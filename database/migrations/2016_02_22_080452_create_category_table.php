<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('slug')->unique();
            $table->integer('parent_id')->nullable()->unsigned();
            $table->string('name');            
            $table->text('desc')->nullable();            
            $table->integer('type')->unsigned();
            $table->integer('attr')->unsigned();
            $table->integer('creator_id')->unsigned()->nullable();
            $table->timestamps();
            $table->index(['slug']);
            $table->softDeletes();
        });
        Schema::table('category', function (Blueprint $table) {
            $table->foreign('parent_id')->references('id')->on('category');
            $table->foreign('creator_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('category');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ImageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('image', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('obj_type')->nullable();
            $table->integer('obj_id')->unsigned()->nullable; 
            $table->string('title')->nullable();
            $table->text('desc')->nullable();
            $table->string('url');
            $table->string('source')->nullable();
            $table->string('sizes');
            $table->timestamps();
            $table->index(['obj_type','obj_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('image');
    }
}

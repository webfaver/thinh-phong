<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('slug')->unique();   
            $table->integer('category_id')->unsigned()->default(0);
            $table->string('brand')->nullable();
            $table->integer('price')->nullable();
            $table->string('name');   
            $table->text('desc');
            $table->string('plu');
            $table->integer('status')->unsigned()->default(0);
            $table->integer('attr')->unsigned()->default(0);
            $table->integer('creator_id')->unsigned();
            $table->integer('type')->unsigned()->default(1);
            $table->timestamps();            
            $table->index(['slug']);
            $table->softDeletes();
        });
        Schema::table('product', function (Blueprint $table) {
            $table->foreign('category_id')->references('id')->on('category');
            $table->foreign('creator_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('product');
    }
}

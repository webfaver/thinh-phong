<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Role;
use App\Models\Permission;
use App\Models\User;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //setup Role
        $admin = new Role();
        $admin->name         = 'admin';
        $admin->display_name = 'Quản trị viên';
        $admin->description  = 'Quản trị viên hệ thống';
        $admin->save();

        $khoHang = new Role();
        $khoHang->name         = 'khohang';
        $khoHang->display_name = 'Quản lý kho hàng';
        $khoHang->description  = 'Quản lý các sản phẩm trong kho hàng';
        $khoHang->save();

        $kinhDoanh = new Role();
        $kinhDoanh->name         = 'kinhdoanh';
        $kinhDoanh->display_name = 'Nhân viên kinh doanh';
        $kinhDoanh->description  = 'Nhân viên kinh doanh';
        $kinhDoanh->save();
        

        //setup User
        $demo1 = new User();
        $demo1->name = 'Demo1';
        $demo1->email = 'demo1@me.com';
        $demo1->password = Hash::make('123456a@');
        $demo1->save();
        $demo1->attachRoles([$admin]);

        $demo2 = new User();
        $demo2->name = 'Demo2';
        $demo2->email = 'demo2@me.com';
        $demo2->password = Hash::make('123456a@');
        $demo2->save();
        $demo2->attachRoles([$khoHang]);

        $demo3 = new User();
        $demo3->name = 'Demo3';
        $demo3->email = 'demo3@me.com';
        $demo3->password = Hash::make('123456a@');
        $demo3->save();
        $demo3->attachRoles([$kinhDoanh]);

        //setup permission
        
        /**
         * Các quyền liên quan đến kho hàng
         * @var Permission
         */
        $createProduct = new Permission();
        $createProduct->name         = 'create-product';
        $createProduct->display_name = 'Thêm sản phẩm';
        $createProduct->description  = 'Thêm sản phẩm';
        $createProduct->save();

        $updateProduct = new Permission();
        $updateProduct->name         = 'update-product';
        $updateProduct->display_name = 'Sửa bài';
        $updateProduct->description  = 'Cập nhật nội dung bài viết';
        $updateProduct->save();

        $deleteProduct = new Permission();
        $deleteProduct->name         = 'delete-product';
        $deleteProduct->display_name = 'Xóa bài';
        $deleteProduct->description  = 'Xóa bài viết';
        $deleteProduct->save();

        $onlineProduct = new Permission();
        $onlineProduct->name         = 'online-product';
        $onlineProduct->display_name = 'Đăng bán sản phẩm';
        $onlineProduct->description  = 'Quyền đăng bán sản phẩm, cho phép sản phẩm xuất hiện trên cửa hàng';
        $onlineProduct->save();

        $supendProduct = new Permission();
        $supendProduct->name         = 'supend-product';
        $supendProduct->display_name = 'Tạm ngừng bán một sản phẩm';
        $supendProduct->description  = 'Quyền tạm ngưng bán một sản phẩm, không hiển thị sản phẩn trên cửa hàng';
        $supendProduct->save();

         /**
         * Các quyền quản lý danh mục sản phẩm
         * @var Permission
         */
        $createGallery = new Permission();
        $createGallery->name         = 'create-gallery';
        $createGallery->display_name = 'Tạo gallery';
        $createGallery->description  = 'Tạo gallery';
        $createGallery->save();

        $editGallery = new Permission();
        $editGallery->name         = 'edit-gallery';
        $editGallery->display_name = 'Sửa gallery';
        $editGallery->description  = 'Sửa gallery';
        $editGallery->save();

        $delGallery = new Permission();
        $delGallery->name         = 'del-gallery';
        $delGallery->display_name = 'Xóa gallery';
        $delGallery->description  = 'Xóa gallery';
        $delGallery->save();

        /**
         * Các quyền liên quan đến quản lý Người Dùng
         * @var Permission
         */
        $createUser = new Permission();
        $createUser->name         = 'create-user';
        $createUser->display_name = 'Tạo người dùng';
        $createUser->description  = 'Tạo tài khoản người dùng mới trên hệ thống';
        $createUser->save();

        $editUser = new Permission();
        $editUser->name         = 'edit-user';
        $editUser->display_name = 'Cập nhật thông tin user';
        $editUser->description  = 'Cập nhật thông tin user, bao gồm mật khẩu';
        $editUser->save();

        $lockUser = new Permission();
        $lockUser->name         = 'lock-user';
        $lockUser->display_name = 'Khóa tài khoản';
        $lockUser->description  = 'Khóa tài khoản người dùng trên hệ thống';
        $lockUser->save();

        $unlockUser = new Permission();
        $unlockUser->name         = 'unlock-user';
        $unlockUser->display_name = 'Mở khóa tài khoản';
        $unlockUser->description  = 'Mở khóa tài khoản';
        $unlockUser->save();

        /**
         * Các quyền của Admin
         * @var Permission
         */
        $editConfig = new Permission();
        $editConfig->name         = 'edit-config';
        $editConfig->display_name = 'Chỉnh sửa cấu hình';
        $editConfig->description  = 'Chỉnh sửa cấu hình';
        $editConfig->save();

        $editRole = new Permission();
        $editRole->name         = 'edit-role';
        $editRole->display_name = 'Thay đổi quyền';
        $editRole->description  = 'Thay đổi/cấp quyền cho các loại tài khoản';
        $editRole->save();

        $khoHang->attachPermissions([
            $createProduct,
            $updateProduct,
            $deleteProduct,
            $onlineProduct,
            $supendProduct,
        ]);
        $admin->attachPermissions([
            $createGallery,
            $editGallery,
            $delGallery,
            $createUser,
            $editUser,
            $lockUser,
            $unlockUser,
            $editConfig,
            $editRole,
        ]);
    }
}

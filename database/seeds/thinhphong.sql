-- Dump data of "category" ---------------------------------
INSERT INTO `category`(`id`,`slug`,`parent_id`,`name`,`desc`,`type`,`attr`,`creator_id`,`created_at`,`updated_at`,`deleted_at`) VALUES ( '3', 'whisky', NULL, 'Whisky', 'Uýt ki (tiếng Anh, tiếng Pháp: Whisky, tại Ireland và phần lớn nước Mỹ là Whiskey) là một loại đồ uống có chứa cồn được sản xuất từ ngũ cốc bằng cách lên men và chưng cất.

Từ Whisky được nhắc đến lần đầu tiên vào năm 1736 xuất phát từ usisge beatha trong tiếng Gaelic tại Scotland hay từ uisce beatha trong tiếng Gaelic tại Ireland và có nghĩa là "nước của cuộc sống" (uisge/uisce: "nước", beatha: "sống"). Khái niệm này đã phổ biến ngay từ thế kỷ 16 / thế kỷ 17 nhưng thời đấy người ta hiểu đấy không chỉ riêng là Whisky mà còn là những loại rượu chưng cất khác có thêm đồ gia vị.', '1', '0', '1', '2016-04-05 18:48:59', '2016-04-07 16:51:30', NULL );
INSERT INTO `category`(`id`,`slug`,`parent_id`,`name`,`desc`,`type`,`attr`,`creator_id`,`created_at`,`updated_at`,`deleted_at`) VALUES ( '4', 'johhnie-walker', '3', 'Johhnie Walker', '', '1', '0', '1', '2016-04-07 16:52:22', '2016-04-07 16:52:22', NULL );
INSERT INTO `category`(`id`,`slug`,`parent_id`,`name`,`desc`,`type`,`attr`,`creator_id`,`created_at`,`updated_at`,`deleted_at`) VALUES ( '5', 'chivas', '3', 'Chivas', '', '1', '0', '1', '2016-04-07 16:53:37', '2016-04-07 16:53:37', NULL );
INSERT INTO `category`(`id`,`slug`,`parent_id`,`name`,`desc`,`type`,`attr`,`creator_id`,`created_at`,`updated_at`,`deleted_at`) VALUES ( '6', 'cognac', NULL, 'Cognac', '', '1', '0', '1', '2016-04-07 16:56:11', '2016-04-07 16:56:11', NULL );
INSERT INTO `category`(`id`,`slug`,`parent_id`,`name`,`desc`,`type`,`attr`,`creator_id`,`created_at`,`updated_at`,`deleted_at`) VALUES ( '7', 'hennessy', '6', 'Hennessy', 'Jas Hennessy& Co hay Hennessy là một công ty sản xuất rượu hàng đầu của Pháp[1] đồng thời cũng là đồng lãnh đạo công ty sản xuất hàng hiệu nổi tiếng Louis Vuitton[2][3]. Đây là công ty sản xuất và kinh doanh rượu, có lịch sử hoạt động lâu đời và cũng là loại rượu yêu thích của nhiều nhân vật yêu thích rượu hàng đầu trên toàn thế giới. Hiện nay, một năm công ty bán khoảng 50 triệu chai rượu.

Nguồn gốc của Hennessy bắt đầu khi một quý tộc người Ailen có tên Richard Hennessy bắt đầu khởi nghiệp tại Cognac, Pháp vào năm 1765[1]. Mảnh đất nơi ông khởi nghiệp sau đó thuộc về sở hữu của Hennessy do những công trạng của ông đóng góp cho vua Louis XV[3].

Cognac là tên của một thành phố ở vùng Charentes của Pháp đồng thời cũng là loại rượu nặng được chế biến từ nho. Người Pháp quen gọi thứ đồ uống đặc sản này là Eau-de-Vie de Cognac/Eau-de-Vie des Charentes[1]. Rượu Cognac là một loại rượu ghép, có niên hạn khác nhau được pha trộn cũng khác nhau, nổi tiếng thế giới với nhiều thương hiệu như: Henessy, Remy Martin, Lainé, Larsen, Bonaparte, Tanier, Berville, Bertrand, Gourmont, Maxime Trijol, St-Rémy…', '1', '0', '1', '2016-04-07 16:56:34', '2016-04-08 09:25:32', NULL );
INSERT INTO `category`(`id`,`slug`,`parent_id`,`name`,`desc`,`type`,`attr`,`creator_id`,`created_at`,`updated_at`,`deleted_at`) VALUES ( '8', 'remy-martin', '6', 'Remy martin', '', '1', '0', '1', '2016-04-07 16:57:49', '2016-04-07 16:57:49', NULL );
INSERT INTO `category`(`id`,`slug`,`parent_id`,`name`,`desc`,`type`,`attr`,`creator_id`,`created_at`,`updated_at`,`deleted_at`) VALUES ( '9', 'camus', '6', 'Camus', '', '1', '0', '1', '2016-04-07 16:59:34', '2016-04-07 16:59:34', NULL );
INSERT INTO `category`(`id`,`slug`,`parent_id`,`name`,`desc`,`type`,`attr`,`creator_id`,`created_at`,`updated_at`,`deleted_at`) VALUES ( '10', 'macallan', '3', 'Macallan', '', '1', '0', '1', '2016-04-07 17:04:22', '2016-04-07 17:04:22', NULL );
INSERT INTO `category`(`id`,`slug`,`parent_id`,`name`,`desc`,`type`,`attr`,`creator_id`,`created_at`,`updated_at`,`deleted_at`) VALUES ( '11', 'ballantine', '3', 'Ballantine', '', '1', '0', '1', '2016-04-07 17:25:51', '2016-04-07 17:25:51', NULL );
INSERT INTO `category`(`id`,`slug`,`parent_id`,`name`,`desc`,`type`,`attr`,`creator_id`,`created_at`,`updated_at`,`deleted_at`) VALUES ( '12', 'glenfiddich', '3', 'Glenfiddich', '', '1', '0', '1', '2016-04-07 17:27:01', '2016-04-07 17:27:01', NULL );
INSERT INTO `category`(`id`,`slug`,`parent_id`,`name`,`desc`,`type`,`attr`,`creator_id`,`created_at`,`updated_at`,`deleted_at`) VALUES ( '13', 'ruou-vang', NULL, 'Rượu vang', '', '1', '0', '1', '2016-04-07 17:29:31', '2016-04-07 17:29:31', NULL );
INSERT INTO `category`(`id`,`slug`,`parent_id`,`name`,`desc`,`type`,`attr`,`creator_id`,`created_at`,`updated_at`,`deleted_at`) VALUES ( '14', 'ruou-mui', NULL, 'Rượu mùi', '', '1', '0', '1', '2016-04-07 17:31:08', '2016-04-07 17:31:08', NULL );
INSERT INTO `category`(`id`,`slug`,`parent_id`,`name`,`desc`,`type`,`attr`,`creator_id`,`created_at`,`updated_at`,`deleted_at`) VALUES ( '15', 'san-pham-khac', NULL, 'Sản phẩm khác', '', '1', '0', '1', '2016-04-07 17:32:52', '2016-04-07 17:32:52', NULL );
INSERT INTO `category`(`id`,`slug`,`parent_id`,`name`,`desc`,`type`,`attr`,`creator_id`,`created_at`,`updated_at`,`deleted_at`) VALUES ( '16', 'banh-keo', '15', 'Bánh kẹo', '', '1', '0', '1', '2016-04-07 17:33:58', '2016-04-07 17:33:58', NULL );
INSERT INTO `category`(`id`,`slug`,`parent_id`,`name`,`desc`,`type`,`attr`,`creator_id`,`created_at`,`updated_at`,`deleted_at`) VALUES ( '17', 'qua-tet', '15', 'Quà tết', '', '1', '0', '1', '2016-04-07 17:35:21', '2016-04-07 17:35:21', NULL );
-- ---------------------------------------------------------


-- Dump data of "config" -----------------------------------
INSERT INTO `config`(`id`,`key`,`value`,`description`,`created_at`,`updated_at`) VALUES ( '1', 'DEFAULT_PASSWORD', '123456a@', NULL, '2016-04-07 18:42:23', '2016-04-07 18:42:23' );
-- ---------------------------------------------------------


-- Dump data of "image" ------------------------------------
INSERT INTO `image`(`id`,`obj_type`,`obj_id`,`title`,`desc`,`url`,`source`,`sizes`,`created_at`,`updated_at`) VALUES ( '1', 'category', '3', 'Aged_Canadian_Whisky.jpg', NULL, '/upload/thumbnails/16/04/05/6DlRfclb5Q.jpg', NULL, '480,360,240,128,88', '2016-04-05 18:48:59', '2016-04-05 18:48:59' );
INSERT INTO `image`(`id`,`obj_type`,`obj_id`,`title`,`desc`,`url`,`source`,`sizes`,`created_at`,`updated_at`) VALUES ( '2', 'product', '1', 'CHIVAS_18_1.jpg', '', '/upload/images/16/04/05/gION1p4J6p.jpg', NULL, '960,720,480,360,240,128,88', '2016-04-05 18:52:30', '2016-04-05 18:52:30' );
INSERT INTO `image`(`id`,`obj_type`,`obj_id`,`title`,`desc`,`url`,`source`,`sizes`,`created_at`,`updated_at`) VALUES ( '3', 'product', '1', '779985491210.jpg', '', '/upload/images/16/04/05/InwHtotdOA.jpg', NULL, '960,720,480,360,240,128,88', '2016-04-05 18:52:30', '2016-04-05 18:52:30' );
INSERT INTO `image`(`id`,`obj_type`,`obj_id`,`title`,`desc`,`url`,`source`,`sizes`,`created_at`,`updated_at`) VALUES ( '4', 'product', '1', '2344203ruou_chivas_18_nam_0932757872.jpg', '', '/upload/images/16/04/05/AIEkDDjMVw.jpg', NULL, '960,720,480,360,240,128,88', '2016-04-05 18:52:31', '2016-04-05 18:52:31' );
INSERT INTO `image`(`id`,`obj_type`,`obj_id`,`title`,`desc`,`url`,`source`,`sizes`,`created_at`,`updated_at`) VALUES ( '5', 'product', '1', '436781246210.jpg', '', '/upload/images/16/04/05/dvWvKV9oEE.jpg', NULL, '1920,1080,960,720,480,360,240,128,88', '2016-04-05 18:52:32', '2016-04-05 18:52:32' );
INSERT INTO `image`(`id`,`obj_type`,`obj_id`,`title`,`desc`,`url`,`source`,`sizes`,`created_at`,`updated_at`) VALUES ( '6', 'category', '4', 'johnniewalker_black750new__58651.1377619442.1280.1280.jpg', NULL, '/upload/thumbnails/16/04/07/5Dw6LJKJPf.jpg', NULL, '960,720,480,360,240,128,88', '2016-04-07 16:52:23', '2016-04-07 16:52:23' );
INSERT INTO `image`(`id`,`obj_type`,`obj_id`,`title`,`desc`,`url`,`source`,`sizes`,`created_at`,`updated_at`) VALUES ( '7', 'category', '5', 'Chivas_Regal_18_Years_Old.jpg', NULL, '/upload/thumbnails/16/04/07/wLKkEdeLK4.jpg', NULL, '960,720,480,360,240,128,88', '2016-04-07 16:53:38', '2016-04-07 16:53:38' );
INSERT INTO `image`(`id`,`obj_type`,`obj_id`,`title`,`desc`,`url`,`source`,`sizes`,`created_at`,`updated_at`) VALUES ( '8', 'category', '6', 'cq5dam.web.1280.1280.jpeg', NULL, '/upload/thumbnails/16/04/07/7S9MbVcVAg.jpeg', NULL, '720,480,360,240,128,88', '2016-04-07 16:56:12', '2016-04-07 16:56:12' );
INSERT INTO `image`(`id`,`obj_type`,`obj_id`,`title`,`desc`,`url`,`source`,`sizes`,`created_at`,`updated_at`) VALUES ( '9', 'category', '7', 'cq5dam.web.1280.1280.jpeg', NULL, '/upload/thumbnails/16/04/07/v0oOJVsDyL.jpeg', NULL, '720,480,360,240,128,88', '2016-04-07 16:56:34', '2016-04-07 16:56:34' );
INSERT INTO `image`(`id`,`obj_type`,`obj_id`,`title`,`desc`,`url`,`source`,`sizes`,`created_at`,`updated_at`) VALUES ( '10', 'category', '8', 'Remy-Martin-1738-Accord-Royal-is-not-273-years-old-1024x977.jpg', NULL, '/upload/thumbnails/16/04/07/kXN5KqXvvv.jpg', NULL, '960,720,480,360,240,128,88', '2016-04-07 16:57:49', '2016-04-07 16:57:49' );
INSERT INTO `image`(`id`,`obj_type`,`obj_id`,`title`,`desc`,`url`,`source`,`sizes`,`created_at`,`updated_at`) VALUES ( '11', 'category', '9', 'CAMUS-VS-ELEGANCE.jpg', NULL, '/upload/thumbnails/16/04/07/eGXZEdC9mK.jpg', NULL, '960,720,480,360,240,128,88', '2016-04-07 16:59:35', '2016-04-07 16:59:35' );
INSERT INTO `image`(`id`,`obj_type`,`obj_id`,`title`,`desc`,`url`,`source`,`sizes`,`created_at`,`updated_at`) VALUES ( '12', 'category', '10', 'macallan-rare-cask-single-malt-scotch-whisky-1.jpg', NULL, '/upload/thumbnails/16/04/07/ba4EGxbVUe.jpg', NULL, '960,720,480,360,240,128,88', '2016-04-07 17:04:22', '2016-04-07 17:04:22' );
INSERT INTO `image`(`id`,`obj_type`,`obj_id`,`title`,`desc`,`url`,`source`,`sizes`,`created_at`,`updated_at`) VALUES ( '13', 'category', '11', '2812.jpg', NULL, '/upload/thumbnails/16/04/07/bIZDZRfxYZ.jpg', NULL, '1080,960,720,480,360,240,128,88', '2016-04-07 17:25:53', '2016-04-07 17:25:53' );
INSERT INTO `image`(`id`,`obj_type`,`obj_id`,`title`,`desc`,`url`,`source`,`sizes`,`created_at`,`updated_at`) VALUES ( '14', 'category', '12', 'glenfiddich_rich_oak_14_year.jpg', NULL, '/upload/thumbnails/16/04/07/5T6dFpR1HU.jpg', NULL, '1080,960,720,480,360,240,128,88', '2016-04-07 17:27:02', '2016-04-07 17:27:02' );
INSERT INTO `image`(`id`,`obj_type`,`obj_id`,`title`,`desc`,`url`,`source`,`sizes`,`created_at`,`updated_at`) VALUES ( '15', 'category', '13', '1211679950812.jpg', NULL, '/upload/thumbnails/16/04/07/NFrpNYv29b.jpg', NULL, '240,128,88', '2016-04-07 17:29:31', '2016-04-07 17:29:31' );
INSERT INTO `image`(`id`,`obj_type`,`obj_id`,`title`,`desc`,`url`,`source`,`sizes`,`created_at`,`updated_at`) VALUES ( '16', 'category', '14', 'rượu-mui-Cointreau.jpg', NULL, '/upload/thumbnails/16/04/07/ZEsG9DJS6m.jpg', NULL, '1080,960,720,480,360,240,128,88', '2016-04-07 17:31:10', '2016-04-07 17:31:10' );
INSERT INTO `image`(`id`,`obj_type`,`obj_id`,`title`,`desc`,`url`,`source`,`sizes`,`created_at`,`updated_at`) VALUES ( '17', 'category', '15', 'b625a266-d70b-446e-894d-2d52018a80e2.jpg', NULL, '/upload/thumbnails/16/04/07/i8bApAAmwZ.jpg', NULL, '480,360,240,128,88', '2016-04-07 17:32:52', '2016-04-07 17:32:52' );
INSERT INTO `image`(`id`,`obj_type`,`obj_id`,`title`,`desc`,`url`,`source`,`sizes`,`created_at`,`updated_at`) VALUES ( '18', 'category', '16', 'khay1jpg_LDMC.jpg', NULL, '/upload/thumbnails/16/04/07/M49g3zPOpB.jpg', NULL, '360,240,128,88', '2016-04-07 17:33:59', '2016-04-07 17:33:59' );
INSERT INTO `image`(`id`,`obj_type`,`obj_id`,`title`,`desc`,`url`,`source`,`sizes`,`created_at`,`updated_at`) VALUES ( '19', 'category', '17', 'alotin.vn_1404266794_806a316849e3673826b6f0e0404e6703.jpg', NULL, '/upload/thumbnails/16/04/07/ZO96etFJqG.jpg', NULL, '480,360,240,128,88', '2016-04-07 17:35:21', '2016-04-07 17:35:21' );
INSERT INTO `image`(`id`,`obj_type`,`obj_id`,`title`,`desc`,`url`,`source`,`sizes`,`created_at`,`updated_at`) VALUES ( '20', 'category', '13', 'ruou-vang-phap1.jpg', NULL, '/upload/thumbnails/16/04/07/pxlqE8t96c.jpg', NULL, '480,360,240,128,88', '2016-04-07 17:36:55', '2016-04-07 17:36:55' );
INSERT INTO `image`(`id`,`obj_type`,`obj_id`,`title`,`desc`,`url`,`source`,`sizes`,`created_at`,`updated_at`) VALUES ( '21', 'product', '2', '01-johnnie-walker-red-label.png', '', '/upload/images/16/04/07/GVsnNJydY6.png', NULL, '720,480,360,240,128,88', '2016-04-07 18:14:45', '2016-04-07 18:14:45' );
INSERT INTO `image`(`id`,`obj_type`,`obj_id`,`title`,`desc`,`url`,`source`,`sizes`,`created_at`,`updated_at`) VALUES ( '22', 'product', '3', '01-johnnie-walker-black-label.png', '', '/upload/images/16/04/07/854Fwmoex6.png', NULL, '720,480,360,240,128,88', '2016-04-07 18:15:00', '2016-04-07 18:15:00' );
INSERT INTO `image`(`id`,`obj_type`,`obj_id`,`title`,`desc`,`url`,`source`,`sizes`,`created_at`,`updated_at`) VALUES ( '23', 'product', '4', '01-johnnie-walker-double-black.png', '', '/upload/images/16/04/07/EjJIPszI0Z.png', NULL, '720,480,360,240,128,88', '2016-04-07 18:15:11', '2016-04-07 18:15:11' );
INSERT INTO `image`(`id`,`obj_type`,`obj_id`,`title`,`desc`,`url`,`source`,`sizes`,`created_at`,`updated_at`) VALUES ( '24', 'product', '5', '01-johnnie-walker-platinum-18-year-old.png', '', '/upload/images/16/04/07/hK0c84LBqr.png', NULL, '720,480,360,240,128,88', '2016-04-07 18:15:31', '2016-04-07 18:15:31' );
INSERT INTO `image`(`id`,`obj_type`,`obj_id`,`title`,`desc`,`url`,`source`,`sizes`,`created_at`,`updated_at`) VALUES ( '25', 'product', '6', 'hennessy-x-futura-limited-edition-cognac.jpg', '', '/upload/images/16/04/07/MPML9I1h1a.jpg', NULL, '720,480,360,240,128,88', '2016-04-07 18:22:19', '2016-04-07 18:22:19' );
INSERT INTO `image`(`id`,`obj_type`,`obj_id`,`title`,`desc`,`url`,`source`,`sizes`,`created_at`,`updated_at`) VALUES ( '26', 'product', '6', 'hennessy-x-futura-limited-edition-cognac (3).jpg', '', '/upload/images/16/04/07/6WQCqwkoxv.jpg', NULL, '720,480,360,240,128,88', '2016-04-07 18:24:48', '2016-04-07 18:24:48' );
INSERT INTO `image`(`id`,`obj_type`,`obj_id`,`title`,`desc`,`url`,`source`,`sizes`,`created_at`,`updated_at`) VALUES ( '27', 'product', '6', 'hennessy-x-futura-limited-edition-cognac (1).jpg', '', '/upload/images/16/04/07/HVsOtzeDjC.jpg', NULL, '720,480,360,240,128,88', '2016-04-07 18:24:48', '2016-04-07 18:24:48' );
INSERT INTO `image`(`id`,`obj_type`,`obj_id`,`title`,`desc`,`url`,`source`,`sizes`,`created_at`,`updated_at`) VALUES ( '28', 'product', '6', 'hennessy-x-futura-limited-edition-cognac (2).jpg', '', '/upload/images/16/04/07/00Xg04b1oQ.jpg', NULL, '720,480,360,240,128,88', '2016-04-07 18:24:49', '2016-04-07 18:24:49' );
INSERT INTO `image`(`id`,`obj_type`,`obj_id`,`title`,`desc`,`url`,`source`,`sizes`,`created_at`,`updated_at`) VALUES ( '29', 'product', '7', 'hennessy-250-collector-blend (1).jpg', '', '/upload/images/16/04/07/67MAAMeifS.jpg', NULL, '720,480,360,240,128,88', '2016-04-07 18:26:35', '2016-04-07 18:26:35' );
INSERT INTO `image`(`id`,`obj_type`,`obj_id`,`title`,`desc`,`url`,`source`,`sizes`,`created_at`,`updated_at`) VALUES ( '30', 'product', '7', 'hennessy-250-collector-blend.jpg', '', '/upload/images/16/04/07/4309RYeiVS.jpg', NULL, '720,480,360,240,128,88', '2016-04-07 18:26:35', '2016-04-07 18:26:35' );
INSERT INTO `image`(`id`,`obj_type`,`obj_id`,`title`,`desc`,`url`,`source`,`sizes`,`created_at`,`updated_at`) VALUES ( '31', 'product', '8', 'hennessy-black-cognac.jpg', '', '/upload/images/16/04/07/m25rtwUn9d.jpg', NULL, '720,480,360,240,128,88', '2016-04-07 18:27:50', '2016-04-07 18:27:50' );
INSERT INTO `image`(`id`,`obj_type`,`obj_id`,`title`,`desc`,`url`,`source`,`sizes`,`created_at`,`updated_at`) VALUES ( '32', 'product', '9', 'hennessy-classivm-cognac.jpg', '', '/upload/images/16/04/07/H4BxJ4TIHs.jpg', NULL, '720,480,360,240,128,88', '2016-04-07 18:29:32', '2016-04-07 18:29:32' );
INSERT INTO `image`(`id`,`obj_type`,`obj_id`,`title`,`desc`,`url`,`source`,`sizes`,`created_at`,`updated_at`) VALUES ( '33', 'product', '10', '12.png', '', '/upload/images/16/04/07/lrEMxx2B4a.png', NULL, '360,240,128,88', '2016-04-07 18:33:19', '2016-04-07 18:33:19' );
INSERT INTO `image`(`id`,`obj_type`,`obj_id`,`title`,`desc`,`url`,`source`,`sizes`,`created_at`,`updated_at`) VALUES ( '34', 'product', '11', 'chivas_regal_extra_product-page-hero.png', '', '/upload/images/16/04/07/4q97652jJL.png', NULL, '360,240,128,88', '2016-04-07 18:34:39', '2016-04-07 18:34:39' );
INSERT INTO `image`(`id`,`obj_type`,`obj_id`,`title`,`desc`,`url`,`source`,`sizes`,`created_at`,`updated_at`) VALUES ( '35', 'product', '12', '18.png', '', '/upload/images/16/04/07/GN1DUk83ag.png', NULL, '360,240,128,88', '2016-04-07 18:35:24', '2016-04-07 18:35:24' );
INSERT INTO `image`(`id`,`obj_type`,`obj_id`,`title`,`desc`,`url`,`source`,`sizes`,`created_at`,`updated_at`) VALUES ( '36', 'product', '13', '25.png', '', '/upload/images/16/04/07/gP7QrlOksg.png', NULL, '360,240,128,88', '2016-04-07 18:36:44', '2016-04-07 18:36:44' );
INSERT INTO `image`(`id`,`obj_type`,`obj_id`,`title`,`desc`,`url`,`source`,`sizes`,`created_at`,`updated_at`) VALUES ( '37', 'product', '14', 'bottle-product.png', '', '/upload/images/16/04/07/qifGILus2i.png', NULL, '360,240,128,88', '2016-04-07 18:38:19', '2016-04-07 18:38:19' );
-- ---------------------------------------------------------


-- Dump data of "product" ----------------------------------
INSERT INTO `product`(`id`,`slug`,`category_id`,`brand`,`price`,`name`,`desc`,`plu`,`status`,`attr`,`creator_id`,`type`,`created_at`,`updated_at`,`deleted_at`) VALUES ( '1', 'ruou-chivas-18-years', '5', 'Rượu Scotland', '3900000', 'Rượu CHIVAS 18 YEARS', '<p>Chivas 18yo Au - Xuất xứ từ Ch&acirc;u &Acirc;u.</p>

<p>Trong c&aacute;c loại&nbsp;Rượu Chivas,&nbsp;<strong>Chivas Regal 18 Year Old</strong>&nbsp;mang đến cho bạn những trải nghiệm về rượu&nbsp;whisky&nbsp;v&ocirc; c&ugrave;ng đ&aacute;ng gi&aacute; bởi v&igrave; đ&acirc;y l&agrave; một loại whisky với nguy&ecirc;n liệu được lựa chọn kỹ c&agrave;ng bằng tay, mang đến một hương thơm ng&aacute;t thật kh&aacute;c biệt.</p>

<p>Cụ thể hơn, những hạt ngũ cốc v&agrave; mạch nha ngon nhất v&agrave; tốt nhất được chọn lựa v&agrave; sau đ&oacute; ch&uacute;ng được ủ ri&ecirc;ng biệt trong những th&ugrave;ng gỗ sồi c&oacute; tuổi từ 18 năm trở l&ecirc;n trong một thời gian nhất định. Cuối c&ugrave;ng ch&uacute;ng được h&ograve;a trộn với nhau để tạo th&agrave;nh Chivas Regal 18.</p>

<p>&nbsp;</p>

<p><strong>Đặc điểm</strong></p>

<ul>
	<li><strong>M&agrave;u</strong>: v&agrave;ng đậm hổ ph&aacute;ch</li>
	<li><strong>M&ugrave;i</strong>: hương thơm nhiều lớp với hương tr&aacute;i c&acirc;y kh&ocirc; v&agrave; kẹo bơ.</li>
	<li><strong>Vị</strong>: nồng n&agrave;n, ngọt ng&agrave;o v&agrave; &ecirc;m diệu một c&aacute;ch kh&aacute;c biệt với vị s&ocirc;c&ocirc;la đắng thoảng hương hoa thanh lịch v&agrave; một &iacute;t hương kh&oacute;i, dư vị k&eacute;o d&agrave;i, ấm &aacute;p v&agrave; đ&aacute;ng nhớ.</li>
</ul>

<p><strong>C&aacute;c thưởng thức</strong></p>

<p>D&ugrave;ng nguy&ecirc;n chất hoặc với đ&aacute;.</p>
', '01WB-CHV0649', '1', '0', '1', '1', '2016-04-05 18:49:37', '2016-04-07 17:51:00', NULL );
INSERT INTO `product`(`id`,`slug`,`category_id`,`brand`,`price`,`name`,`desc`,`plu`,`status`,`attr`,`creator_id`,`type`,`created_at`,`updated_at`,`deleted_at`) VALUES ( '2', 'johnnie-walker-r-red-label-r', '4', 'JOHNNIE WALKER', '0', 'JOHNNIE WALKER® RED LABEL®', '<h2>JOHNNIE WALKER&reg; RED LABEL&reg;</h2>

<p>L&agrave; d&ograve;ng Whisky ti&ecirc;n phong vươn ra khỏi l&atilde;nh thổ Scotland v&agrave; trở th&agrave;nh d&ograve;ng Scotch Whisky b&aacute;n chạy nhất thế giới. Tuyệt phẩm JOHNNIE WALKER&reg; RED LABEL&reg; c&oacute; thể sử dụng linh loạt với c&aacute;c loại thức uống kh&aacute;c nhưng vẫn giữ được c&aacute; t&iacute;nh v&agrave; hương vị mạnh mẽ của Scotch Whisky truyền thống. Đ&acirc;y sẽ l&agrave; sự lựa chọn ho&agrave;n hảo cho những bữa tiệc họp mặt tưng bừng c&ugrave;ng bạn b&egrave;.</p>

<h2>C&Aacute;CH THƯỞNG THỨC</h2>

<p>Bạn c&oacute; thể thưởng thức JOHNNIE WALKER&reg; RED LABEL&reg; theo c&aacute;ch m&agrave; bạn y&ecirc;u th&iacute;ch: uống nguy&ecirc;n chất, d&ugrave;ng với đ&aacute; hoặc pha với c&aacute;c loại thức uống kh&aacute;c hợp khẩu vị của m&igrave;nh.</p>

<p>Một trong những c&aacute;ch thưởng thức đ&uacute;ng điệu của</p>

<p>JOHNNIE WALKER&reg; RED LABEL&reg; l&agrave; kết hợp với Ginger Ale.</p>

<p><img alt="Cocktail Johnnie Ginger trong ly cao" src="https://www.johnniewalker.com/media/1688/01-hero-johnnie-walker-johnnie-and-ginger-ribbon-hiball.jpg" style="height:180px; width:320px" /></p>

<ol>
	<li>Cho đầy đ&aacute; v&agrave;o ly cao</li>
	<li>R&oacute;t&nbsp;25&nbsp;ml&nbsp;JOHNNIE WALKER&reg; RED LABEL&reg;&nbsp;</li>
	<li>Cho v&agrave;o 7 ml nước cốt chanh</li>
	<li>Th&ecirc;m 75 ml Ginger Ale cho đến khi đầy ly.</li>
	<li>Đừng qu&ecirc;n trang tr&iacute; th&ecirc;m bằng một l&aacute;t gừng ở ph&iacute;a tr&ecirc;n</li>
	<li>H&atilde;y&nbsp;c&ugrave;ng&nbsp;thưởng&nbsp;thức!</li>
</ol>
', 'JWRL', '1', '0', '1', '1', '2016-04-07 18:03:29', '2016-04-07 18:03:29', NULL );
INSERT INTO `product`(`id`,`slug`,`category_id`,`brand`,`price`,`name`,`desc`,`plu`,`status`,`attr`,`creator_id`,`type`,`created_at`,`updated_at`,`deleted_at`) VALUES ( '3', 'johnnie-walker-r-black-label-r', '4', '', '0', 'JOHNNIE WALKER® BLACK LABEL®', '<h2>JOHNNIE WALKER<sup>&reg;</sup>&nbsp;BLACK LABEL<sup>&reg;</sup></h2>

<p>JOHNNIE WALKER&reg; BLACK LABEL&reg; 12 Năm Tuổi được mệnh danh l&agrave; &lsquo;Đỉnh Everest Của Whisky Thượng Hạng&rsquo; v&agrave; l&agrave; chuẩn mực của Whisky cao cấp. Được chế t&aacute;c từ c&aacute;c loại Whisky 12 năm tuổi ở 4 v&ugrave;ng trứ danh ở Scotland, JOHNNIE WALKER&reg; BLACK LABEL&reg; 12 Năm Tuổi l&agrave; một tuyệt phẩm ho&agrave;n hảo được kết hợp giữa sự mượt m&agrave;, s&acirc;u lắng trong hương vị v&agrave; sự tinh tế trong t&iacute;nh c&aacute;ch. JOHNNIE WALKER&reg; BLACK LABEL&reg; 12 Năm Tuổi sẽ l&agrave; sự lựa chọn ấn tượng để n&acirc;ng ly cho bất cứ c&aacute;c dịp ăn mừng tại gia hay những buổi tiệc li&ecirc;n hoan đ&aacute;ng nhớ.</p>

<h2>C&Aacute;CH THƯỞNG THỨC</h2>

<p>Bạn c&oacute; thể thưởng thức JOHNNIE WALKER&reg; BLACK LABEL&reg; 12 Năm Tuổi theo c&aacute;ch m&agrave; bạn y&ecirc;u th&iacute;ch: uống nguy&ecirc;n chất, với nước kho&aacute;ng tinh khiết hoặc pha với c&aacute;c loại thức uống hợp khẩu vị của m&igrave;nh.<br />
<br />
Một trong những c&aacute;ch thưởng thức đ&uacute;ng điệu của JOHNNIE WALKER&reg; BLACK LABEL&reg; 12 Năm Tuổi l&agrave; kết hợp với Soda</p>

<p>&nbsp;</p>

<p><img alt="Cocktail Johnnie Walker Black and Soda trong ly cao" src="https://www.johnniewalker.com/media/1694/johnnie-walker-black-and-soda-hiball.jpg" style="height:320px; width:569px" /></p>

<p>Cho đầy đ&aacute; v&agrave;o ly cao.</p>

<p>R&oacute;t&nbsp;25&nbsp;ml&nbsp;JOHNNIE WALKER&reg; BLACK LABEL&reg;&nbsp;12&nbsp;Năm&nbsp;Tuổi</p>

<p>Th&ecirc;m v&agrave;o 75 ml soda</p>

<p>Đừng qu&ecirc;n trang tr&iacute; th&ecirc;m bằng một l&aacute;t chanh tươi ở ph&iacute;a tr&ecirc;n</p>

<p>H&atilde;y&nbsp;c&ugrave;ng&nbsp;thưởng&nbsp;thức</p>
', '', '1', '1', '1', '1', '2016-04-07 18:07:05', '2016-04-08 05:04:05', NULL );
INSERT INTO `product`(`id`,`slug`,`category_id`,`brand`,`price`,`name`,`desc`,`plu`,`status`,`attr`,`creator_id`,`type`,`created_at`,`updated_at`,`deleted_at`) VALUES ( '4', 'johnnie-walker-r-double-black-tm', '4', '', '0', 'JOHNNIE WALKER® DOUBLE BLACK™', '<h2>JOHNNIE WALKER<sup>&reg;</sup>DOUBLE BLACK<sup>&trade;</sup></h2>

<p>JOHNNIE WALKER&reg; DOUBLE BLACK&trade; được chế t&aacute;c cho những người s&agrave;nh Whisky v&agrave; y&ecirc;u hương vị mạnh mẽ, đậm m&ugrave;i kh&oacute;i hơn JOHNNIE WALKER&reg; BLACK LABEL&reg;. C&aacute;c bậc thầy pha chế của JOHNNIE WALKER&reg; đ&atilde; kết hợp th&agrave;nh c&ocirc;ng hương thơm mạnh mẽ nhuốm vị kh&oacute;i kh&ocirc; tự nhi&ecirc;n của Whisky v&ugrave;ng Bờ T&acirc;y Scotland với loại Whisky chưng cất trong th&ugrave;ng gỗ sồi l&acirc;u năm, tạo n&ecirc;n d&ograve;ng Whisky mới s&acirc;u lắng hơn, vị kh&oacute;i đậm hơn, mạnh mẽ hơn, kh&ocirc;ng nhầm lẫn v&agrave;o đ&acirc;u được. JOHNNIE WALKER&reg; DOUBLE BLACK&trade; lu&ocirc;n l&agrave; sự lưa chọn tuyệt vời để chia sẻ cho c&aacute;c dịp ăn mừng v&agrave; li&ecirc;n hoan c&ugrave;ng bạn b&egrave;.</p>

<h2>C&Aacute;CH THƯỞNG THỨC</h2>

<p>C&oacute; nhiều sự lựa chọn trong c&aacute;ch thưởng thức JOHNNIE WALKER&reg; DOUBLE BLACK&trade;- uống nguy&ecirc;n chất, d&ugrave;ng với nước lọc hay kết hợp với c&aacute;c loại thức uống m&agrave; bạn y&ecirc;u th&iacute;ch.</p>

<p>Một trong những c&aacute;ch thưởng thức đ&uacute;ng điệu của JOHNNIE WALKER&reg; DOUBLE BLACK&trade; l&agrave; kết hợp với &iacute;t đ&aacute; tinh khiết.</p>

<p>&nbsp;</p>

<p><img alt="Rượu whisky Johnnie Walker Double Black với đá trong ly tumbler" src="https://www.johnniewalker.com/media/1706/johnnie-walker-double-black-on-the-rocks-tumbler.jpg" style="height:320px; width:569px" /></p>

<ol>
	<li>Cho đầy đ&aacute; vu&ocirc;ng v&agrave;o ly&nbsp;</li>
	<li>R&oacute;t 50 ml&nbsp;JOHNNIE WALKER&reg; DOUBLE BLACK&trade;</li>
	<li>V&agrave; bạn đ&atilde; sẳn s&agrave;ng n&acirc;ng ly c&ugrave;ng bạn b&egrave;</li>
</ol>
', '', '1', '1', '1', '1', '2016-04-07 18:10:57', '2016-04-08 03:37:57', NULL );
INSERT INTO `product`(`id`,`slug`,`category_id`,`brand`,`price`,`name`,`desc`,`plu`,`status`,`attr`,`creator_id`,`type`,`created_at`,`updated_at`,`deleted_at`) VALUES ( '5', 'johnnie-walker-r-platinum-label-tm-18-nam-tuoi', '4', '', '0', 'JOHNNIE WALKER® PLATINUM LABEL™ 18 NĂM TUỔI', '<h2>JOHNNIE WALKER<sup>&reg;</sup>PLATINUM LABEL<sup>&trade;</sup>&nbsp;18 NĂM TUỔI</h2>

<p>Được truyền cảm hứng từ truyền thống nh&agrave; John Walker &amp; Sons chuy&ecirc;n chế t&aacute;c c&aacute;c loại Whisky d&agrave;nh ri&ecirc;ng cho c&aacute;c sự kiện đặc biệt, JOHNNIE WALKER&reg; PLATINUM LABEL&trade; 18 Năm Tuổi được tuyển chọn từ những loại Whisky được ủ trong &iacute;t nhất 18 năm v&agrave; l&agrave; kết tinh của kỹ nghệ chế t&aacute;c truyền thống v&agrave; hiện đại trong khi vẫn giữ được hương vị tinh t&uacute;y của d&ograve;ng JOHNNIE WALKER&reg;. JOHNNIE WALKER&reg; PLATINUM LABEL&trade; 18 năm tuổi l&agrave; sự lựa chọn tuyệt vời d&agrave;nh cho những tiệc đ&ecirc;m thượng đỉnh.</p>

<h2>C&Aacute;CH THƯỞNG THỨC</h2>

<p>Bạn c&oacute; thể thưởng thức JOHNNIE WALKER&reg; PLATINUM LABEL&trade; 18 Năm Tuổi theo c&aacute;ch m&agrave; bạn th&iacute;ch: uống nguy&ecirc;n chất, k&egrave;m đ&aacute; lạnh hoặc th&ecirc;m một &iacute;t nước lọc tinh khiết.</p>

<p><img alt="Johnnie Walker Platinum Label Signature phục vụ hoàn hảo trong ly tumbler" src="https://www.johnniewalker.com/media/1722/johnnie-walker-platinum-label-signature-serve.jpg" style="height:320px; width:569px" /></p>

<ol>
	<li>R&oacute;t&nbsp;45ml&nbsp;JOHNNIE WALKER&reg; PLATINUM LABEL&trade;&nbsp;18&nbsp;Năm&nbsp;Tuổi&nbsp;v&agrave;o&nbsp;ly&nbsp;Whisky</li>
	<li>Thưởng thức nguy&ecirc;n chất hoặc&nbsp;c&ugrave;ng&nbsp;một&nbsp;&iacute;t&nbsp;đ&aacute;&nbsp;lạnh</li>
</ol>
', '', '1', '3', '1', '1', '2016-04-07 18:14:21', '2016-04-08 03:37:16', NULL );
INSERT INTO `product`(`id`,`slug`,`category_id`,`brand`,`price`,`name`,`desc`,`plu`,`status`,`attr`,`creator_id`,`type`,`created_at`,`updated_at`,`deleted_at`) VALUES ( '6', 'futura-x-hennessy-vs-cognac-cognac', '7', '', '0', 'Futura x Hennessy VS Cognac Cognac', '<p>Futura x Hennessy is a limited edition by street artist Futura. The cognac is a VS quality (very special). The special edition even included a set with Chucks and the bottle.</p>
', '', '1', '1', '1', '1', '2016-04-07 18:21:55', '2016-04-08 04:00:18', NULL );
INSERT INTO `product`(`id`,`slug`,`category_id`,`brand`,`price`,`name`,`desc`,`plu`,`status`,`attr`,`creator_id`,`type`,`created_at`,`updated_at`,`deleted_at`) VALUES ( '7', 'hennessy-250-collector-blend-cognac', '7', '', '0', 'Hennessy 250 Collector Blend Cognac', '<p>Created by notorious&nbsp;Cellar Master Yann Fillioux, the&nbsp;<a href="http://blog.cognac-expert.com/distilling-cognac-pot-still-alembic-charentais-french/" title="More about eaux-de-vie »">eaux-de-vie</a>&nbsp;that make up the blend have been aging in 250 handmade, specially commissioned 250 litre Limousin&nbsp;<a href="http://blog.cognac-expert.com/oak-barrel-cask-cognac-age-limousin/" title="More about oak »">oak</a>&nbsp;barrels. Before the eaux-de-vie&nbsp;were placed in these, they&nbsp;had already been aged for at least a decade under &lsquo;optimum conditions&rsquo;.</p>
', '', '1', '1', '1', '1', '2016-04-07 18:26:09', '2016-04-08 03:37:00', NULL );
INSERT INTO `product`(`id`,`slug`,`category_id`,`brand`,`price`,`name`,`desc`,`plu`,`status`,`attr`,`creator_id`,`type`,`created_at`,`updated_at`,`deleted_at`) VALUES ( '8', 'hennessy-black-cognac', '7', '', '0', 'Hennessy Black Cognac', '<p>Hennessy Black was introduced to the US market in 2010. Hennessy Black is known to be a mixable cognac. Is Hennessy a VSOP Cognac, or a VS Cognac, older or quite young? It can be assumed that Black is a VSOP.</p>
', '', '1', '5', '1', '1', '2016-04-07 18:27:36', '2016-04-08 03:36:28', NULL );
INSERT INTO `product`(`id`,`slug`,`category_id`,`brand`,`price`,`name`,`desc`,`plu`,`status`,`attr`,`creator_id`,`type`,`created_at`,`updated_at`,`deleted_at`) VALUES ( '9', 'hennessy-classivm-cognac', '7', '', '0', 'Hennessy Classivm Cognac', '<p>Hennessy Classivm is a Grande and Petite Champagne Cognac. Classivm is a cognac &quot;with a new style&quot; as Hennessy states, destined for the Chinese market. Where else to buy Hennessy Classivm remains a question. The price of Classivm is also something that remains slightly unclear for now. We would love to find out. How mucht does Hennessy Classivm cost? We learned that RMB 450-550 might be the (trade retail) price, that is about 60 . Another question we would (of course) have, is how old Hennessy Classivm is.</p>
', '', '1', '0', '1', '1', '2016-04-07 18:29:18', '2016-04-07 18:29:18', NULL );
INSERT INTO `product`(`id`,`slug`,`category_id`,`brand`,`price`,`name`,`desc`,`plu`,`status`,`attr`,`creator_id`,`type`,`created_at`,`updated_at`,`deleted_at`) VALUES ( '10', 'chivas-12', '5', '', '0', 'Chivas 12', '<p>Chivas&nbsp;is a blend of many different maltand grain Scotch whiskies, matured for at least 12 years. This rich, smooth blend balances style with substance and tradition with a modern twist.&nbsp;</p>
', '', '1', '0', '1', '1', '2016-04-07 18:33:10', '2016-04-07 18:33:10', NULL );
INSERT INTO `product`(`id`,`slug`,`category_id`,`brand`,`price`,`name`,`desc`,`plu`,`status`,`attr`,`creator_id`,`type`,`created_at`,`updated_at`,`deleted_at`) VALUES ( '11', 'chivas-regal-extra', '5', '', '0', 'Chivas Regal Extra', '<p>As the newest member to our family, Chivas Regal Extra will satisfy the desires of the discerning whisky drinker with something exceptional - deep in aroma, rich in fruitiness and generous in sweetness.&nbsp;</p>

<p>&nbsp;</p>

<p>It&rsquo;s created with an exceptional blend of rare whiskies combined with malt whiskies aged in sherry casks from the Oloroso sherry bodegas in Spain.</p>
', '', '1', '0', '1', '1', '2016-04-07 18:34:01', '2016-04-07 18:34:01', NULL );
INSERT INTO `product`(`id`,`slug`,`category_id`,`brand`,`price`,`name`,`desc`,`plu`,`status`,`attr`,`creator_id`,`type`,`created_at`,`updated_at`,`deleted_at`) VALUES ( '12', 'chivas-18', '5', '', '0', 'Chivas 18', '<p>Chivas 18 is meticulously blended by Master Blender Colin Scott. It&rsquo;s a uniquely rich and multi-layered whisky which is produced using various malt and grain whiskies from distilleries all over Scotland, including our classic Speyside malt from Strathisla. Much more than a drink, Chivas 18 is a luxuriously complex experience waiting to be discovered.</p>
', '', '1', '1', '1', '1', '2016-04-07 18:35:06', '2016-04-08 03:36:11', NULL );
INSERT INTO `product`(`id`,`slug`,`category_id`,`brand`,`price`,`name`,`desc`,`plu`,`status`,`attr`,`creator_id`,`type`,`created_at`,`updated_at`,`deleted_at`) VALUES ( '13', 'chivas-regal-25-years-old', '5', '', '0', 'Chivas Regal 25 years old ', '<p>Chivas 25 is a rare and exclusive blend of the finest Scotch whiskies, which have all been aged for a minimum of 25 years. Chivas Regal 25 Year Old is available only as a strictly limited release in individually numbered bottles.</p>
', '', '1', '0', '1', '1', '2016-04-07 18:36:38', '2016-04-07 18:36:38', NULL );
INSERT INTO `product`(`id`,`slug`,`category_id`,`brand`,`price`,`name`,`desc`,`plu`,`status`,`attr`,`creator_id`,`type`,`created_at`,`updated_at`,`deleted_at`) VALUES ( '14', 'chivas-regal-the-icon', '5', '', '0', 'Chivas Regal The Icon', '<blockquote>Chivas Regal The Icon is a truly exquisite blend &ndash; perfectly smooth with an intense concentration of sumptuous flavours</blockquote>

<p style="text-align:center"><strong>Colin Scott, Chivas Master Blender</strong></p>

<h2 style="text-align:center"><img alt="product-hero-icon-logo" src="http://www.chivas.com/en/int/-/media/Chivas/global/images/the-icon/icon-logo.png" style="height:91px; width:137px" /></h2>

<p>Only released in limited quantities each year, Chivas Regal The Icon is the pinnacle of the Chivas Regal range. A deep, complex blend of more than 20 of Scotland&rsquo;s rarest whiskies, it includes some from distilleries now lost forever. A skilfully balanced blend presented by Colin Scott in a handcrafted crystal decanter, it is a symbol of luxury for a new era.</p>
', '', '1', '1', '1', '1', '2016-04-07 18:38:00', '2016-04-08 03:35:43', NULL );
-- ---------------------------------------------------------


-- Dump data of "product_size" -----------------------------
INSERT INTO `product_size`(`id`,`product_id`,`name`,`price`) VALUES ( '6', '8', '1000ml', '0' );
INSERT INTO `product_size`(`id`,`product_id`,`name`,`price`) VALUES ( '7', '7', '750ml', '50000' );
INSERT INTO `product_size`(`id`,`product_id`,`name`,`price`) VALUES ( '8', '6', '700ml', '50000' );
-- ---------------------------------------------------------




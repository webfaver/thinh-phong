function mq(size){
  switch(size) {
    case "xs":
      return Modernizr.mq('only screen and (max-width: 480px)');
    case "sm":
      return Modernizr.mq('only screen and (min-width: 768px) and (max-width: 992px)');
    case "md":
      return Modernizr.mq('only screen and (min-width: 992px) and (max-width: 1200px)');
    case "lg":
      return Modernizr.mq('only screen and (min-width: 1200px)');
  }
}
$('#category li').mouseover(function(){
  $(this).find('.subCategory').stop().slideDown(100);
  
})
$('#category li').mouseout(function(){
  $(this).find('.subCategory').stop().slideUp(100);
  
})


$(document).ready(function(){
  initCartItems();
  loadCart();
  $(document).on('click', '.btn-delete', function(){
    var id = $(this).attr('item-id');
    var sizeId = $(this).attr('item-size');
    
    deleteCartItem(id,sizeId);
  });
  checkOver18();
});
    
function initCartItems(){
    if(isCartEmpty()){
        localStorage.setItem("items",'[]');
    }
}

function emptyCart() {
  $('.cartTable').html('');
  $('.btn-action').hide();
  var tempDiv = "";
  tempDiv+="<div class='cart_box_content_item'>";
  tempDiv+="<p style='text-align:center'>Mời bạn chọn sản phẩm yêu thích</p>";
  tempDiv+="</div>";
  
  $('.cartTable').append(tempDiv);
}

function loadCart(){
  if (isCartEmpty()) {
    $('.cart-box span').text(0);
    emptyCart();
    return;
  }

  $('.cartTable').html('');
  $('.btn-action').show();
  items = JSON.parse(localStorage.getItem("items"));
  if (isCartEmpty()) {
    emptyCart();
    return;
  }
  for(var i = 0; i < items.length; i++){
    var item = items[i].item
    var tempDiv = "";
    tempDiv +='<table class="table cartItems" item-id="'+item.id+'" item-size="'+items[i].size+'">';
      tempDiv +='<tr class="itemsBox">';
        tempDiv +='<td class="col-xs-4 item-img">';
        tempDiv +='<img src="'+item.url+'">';
        tempDiv +='</td>';
        tempDiv +='<td class="item-info">';
          tempDiv +='<div class="item-name">'+item.name+'</div>';
          tempDiv +='<div class="item-size">Cỡ:'+items[i].sizeName+'</div>';
          tempDiv +='<div class="item-quantity">Số lượng:'+items[i].quantity+'</div>';
          tempDiv +='<div class="item-price">Giá:'+(item.price==0?"Liên hệ":item.price)+'</div>';
        tempDiv +='</td>';
        tempDiv +='<td class="btn-delete" item-id="'+item.id+'" item-size="'+items[i].size+'">';
        tempDiv +='<i class="fa fa-times"></i>';
        tempDiv +='</td>';
      tempDiv +='</tr>';
    tempDiv +='</table>';
    
    $('.cartTable').prepend(tempDiv);
  }

  $('.cart-box span').text(items.length);
}

function deleteCartItem(id,sizeId) {
  $('table.cartItems[item-id="'+id+'"][item-size="'+sizeId+'"]').remove();
  $('tr[item-id="'+id+'"][item-size="'+sizeId+'"]').remove();
  var temp = [];
  var storage = JSON.parse(localStorage.getItem('items'));
  
  for(var i=0;i< storage.length;i++){
    if (storage[i].item.id != id) {
      console.log(id +":"+sizeId)
      temp.push(storage[i]);
    }
    else{
      if (storage[i].size != sizeId) {
        temp.push(storage[i]);
      }
    }
  }
  localStorage['items'] = JSON.stringify(temp);
  loadCart();
}

function isCartEmpty() {
  var items = localStorage.getItem("items");
  var isValid = items == null || items == '' || items == '[]';
  return isValid;
}
function isExistsItem(product, size) {
  var items = JSON.parse(localStorage.items);
  if (isCartEmpty())
    return false;
  for(var i = 0; i < items.length; i++) {
    if (items[i].item.id == product.id && items[i].size == size) {
      return true;
    }
  }
  return false;

}
function addToCart(product, size,sizeName, quantity){
  if (isCartEmpty()) {
    initCartItems();
  }
  var items =  JSON.parse(localStorage.items);
  if (isExistsItem(product, size)) {
    //San pham da co trong gio hang
    return false;
  } else {
    items.push({'item':product,'size':size,'sizeName':sizeName, 'quantity':quantity});
  }
  localStorage.items = JSON.stringify(items);
  loadCart()
  return true;
}

$('.openSize').click(function(){
  if ($(this).hasClass('contact')) {
    window.location.href = "/contact";
    return;
  }
  var objectArray = $(this).next().val();
  var productInfo = JSON.parse(objectArray);

  openSizePopup(productInfo);
})
function openSizePopup(product) {
  $('.addCart-popup').fadeIn();
  $('.addCart-popup').find('input.popup-data').val(JSON.stringify(product));
  $('.option-box').attr('id',product.id);
  
  $('.size-box').html('');
  if (product.size.length > 0) {
    for(var i =0;i < product.size.length; i++){
      var size = product.size[i];
      
      var tempDiv = "";
      tempDiv +='<div class="radio">';
        tempDiv +='<input type="radio" id='+size.id+' size-price='+size.price+'>';
        tempDiv +='<label for="'+size.id+'">';
          tempDiv +=''+size.name+'';
        tempDiv +='</label>';
      tempDiv +='</div>';
      
      $('.size-box').append(tempDiv);
    }
  }
}

$('.add').click(function(){
  var productId = $('.option-box').attr('id');
  AddCartPopup(productId);
})

function AddCartPopup(productId) {
  var orderQuantity = $('#quantity').val();
  var sizeId = $('.radio.active input').attr('id');
  var sizeName = $('.radio.active label').text();
  var objectArray = $('.addCart-popup input.popup-data').val();
  var productInfo = JSON.parse(objectArray);
  if (orderQuantity=="" || sizeId=="" || orderQuantity== undefined || sizeId==undefined || orderQuantity <= 0) {
    alert("Hãy chọn size và số lượng phải > 0")
  }else{
    var result = addToCart(productInfo, sizeId,sizeName, orderQuantity);
    if (result) {
      $.notify('Đã thêm vào danh sách. Bấm giỏ hàng trên menu để xem!', 'success');
    } else {
      $.notify('Sản phẩm đã có sẵn trong giỏ.', 'warn');
    }
    $('.addCart-popup').hide();
  }
}

$(document).on('click','.size-box .radio',function(){
  $('.option-box .radio').removeClass('active');
  $(this).addClass('active');
})

$('.addCart-popup .overlay').click(function(){
  $('.addCart-popup').fadeOut();
})


$('#cart-box').click(function(){
  $('.cartContent').slideToggle();
})

$('.over18').click(function(){
  $('.popup').hide();
  localStorage.setItem('over18',true);
})


function checkOver18() {
  if(localStorage.getItem("over18") == null || localStorage.getItem("over18") == undefined){
      $('.popup').show();
      localStorage.setItem("over18",false);
  }
  else{
    var age = JSON.parse(localStorage['over18']);
    if (age == true) {
      $('.popup').hide();
    }
    else{
      $('.popup').show();
    }
  }
}

$('.mobile-btn-menu').click(function(){
  $(this).toggleClass('active');
  $('.menu-nav').toggleClass('active');
})

$('.submenu-toggle').click(function(e){
  e.preventDefault();
  $(this).parent().toggleClass('active');
  $(this).parents('li').find('.mobile-submenu').stop().slideToggle();
})
$('.mobile-menu-list a').click(function(){
  $(this).toggleClass('active');
  $('.mobile-menu-list ul').stop().slideToggle();
})
